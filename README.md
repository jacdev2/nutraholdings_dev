## Installation

* Step 1. Create a new database:
    * Import database from `db/db.sql`
* Step 2. Update environment variables in the `.env` file:
  * Database variables
    * `DB_NAME` - Database name
    * `DB_USER` - Database user
    * `DB_PASSWORD` - Database password
* Step 3. Update `wp_options` table (home and siteurl)
* Step 4. npm & gulp command instructions
  * `npm install -g npm@latest`
  * `npm install -g gulp bower`
  * `npm install`
  * `bower install`

## Troublshooting

NOTE: If getting error: Cannot find module `gulp-sass` then change gulp-sass version in package.json to `^3.0.0`

## Add new SASS file
  1. For Google Fonts - In main.scss import above `settings` `@import url(http://fonts.googleapis.com/css?family=Lato:300,700);`
  2. For Foundation Icon: Download icon form https://zurb.com/playground/foundation-icon-fonts-3; Uncompress the .zip and put them in your project at /fonts/foundation-icons 
  3. Then update main.scss

      `@import "../fonts/foundation-icons/foundation-icons.css";`

      `$font-path: "../fonts/foundation-icons/foundation-icons";
      @font-face {
        font-family: "foundation-icons";
        src: url("#{$font-path}.eot");
        src: url("#{$font-path}.eot?#iefix") format("embedded-opentype"),
             url("#{$font-path}.woff") format("woff"),
             url("#{$font-path}.ttf") format("truetype"),
             url("#{$font-path}.svg#fontcustom") format("svg");
        font-weight: normal;
        font-style: normal;
      }`
  4. For Custom SASS 
      * Create `_custom.scss` in `assets/style/layout`
      * Import `@import "layouts/custom";` in main.scss

  5. Run commend `gulp && gulp watch`
      