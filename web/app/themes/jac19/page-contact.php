<div class="section-split section-contact-map contact-page">
    <?php
        //St. John's
        $address1 = get_option('company_info_company_one_address1');
        $address2 = get_option('company_info_company_one_address2');
        $city = stripslashes(get_option('company_info_company_one_city'));
        $province = get_option('company_info_company_one_province');
        $country = get_option('company_info_company_one_country');
        $postal = get_option('company_info_company_one_postal');
        $phone = get_option('company_info_company_one_phone');
        $fax = get_option('company_info_company_one_fax');
        $tollfree = get_option('company_info_company_one_tollfree');
        $email = get_option('company_info_company_one_email');

        //DepartmentOne
        $departmentonename = get_option('company_info_department_name_one');
        $departmentonetime1 = get_option('company_info_department_one_time_one');
        $departmentonelabel1 = get_option('company_info_department_one_label_one');
        $departmentonetime2 = get_option('company_info_department_one_time_two');
        $departmentonelabel2 = get_option('company_info_department_one_label_two');
        $departmentonetime3 = get_option('company_info_department_one_time_three');
        $departmentonelabel3 = get_option('company_info_department_one_label_three');
        $departmentonetime4 = get_option('company_info_department_one_time_four');
        $departmentonelabel4 = get_option('company_info_department_one_label_four');
        $departmentonetime5 = get_option('company_info_department_one_time_five');
        $departmentonelabel5 = get_option('company_info_department_one_label_five');
        $departmentonetime6 = get_option('company_info_department_one_time_six');
        $departmentonelabel6 = get_option('company_info_department_one_label_six');
        $departmentonetime7 = get_option('company_info_department_one_time_seven');
        $departmentonelabel7 = get_option('company_info_department_one_label_seven');
        $departmentonealert = get_option('company_info_department_one_alert');

        //DepartmentTwo  
        $departmenttwoname = get_option('company_info_department_name_two');
        $departmenttwotime1 = get_option('company_info_department_two_time_one');
        $departmenttwolabel1 = get_option('company_info_department_two_label_one');
        $departmenttwotime2 = get_option('company_info_department_two_time_two');
        $departmenttwolabel2 = get_option('company_info_department_two_label_two');
        $departmenttwotime3 = get_option('company_info_department_two_time_three');
        $departmenttwolabel3 = get_option('company_info_department_two_label_three');
        $departmenttwotime4 = get_option('company_info_department_two_time_four');
        $departmenttwolabel4 = get_option('company_info_department_two_label_four');
        $departmenttwotime5 = get_option('company_info_department_two_time_five');
        $departmenttwolabel5 = get_option('company_info_department_two_label_five');
        $departmenttwotime6 = get_option('company_info_department_two_time_six');
        $departmenttwolabel6 = get_option('company_info_department_two_label_six');
        $departmenttwotime7 = get_option('company_info_department_two_time_seven');
        $departmenttwolabel7 = get_option('company_info_department_two_label_seven');
        $departmenttwoalert = get_option('company_info_department_two_alert');

        //holidays
        $holiday = get_option('company_info_company_one_holiday');

        if($address1){
            $address = $address1.', ';
        } elseif($address2){
            $address = $address1.', ';
        } else{}
        if($province){
            $province = $province;
        } 
        if($country){
            $country = ', '. $country;
        } 
        if(!$province) { $province = 'NL'; }
        
        if($address1){
            $address = $address1;
        } else  {
            $address = $address2;
        }
    ?>
    <?php
        if($address && $city) { ?>
            <div class="split-col split-image bg-secondary">
            <div
                class="gmapcontact"
                data-zoom="16"
                data-address="<?= implode(', ', [$address, $city, $province, $country]) ?>"
            ></div>
            </div><!--/.split-image-->
            <script src="//maps.googleapis.com/maps/api/js?key=<?= get_option('google_api_key') ?>"></script>
        <?php } 
    ?>
    <div class="split-col split-content">
        <div class="overview-color">
            <div class="section container">
                <div class="content-contact">
                    <?php the_subtitle('<h2>','</h2>')?>
                    <?php the_excerpt()?>
                    <?php the_content()?>
                </div>
            </div><!--.container-->
            <div class="contact-box">
                <div class="section-split container">
                <div class="map-holder split-col">
                    <?php
                    if($address && $city) { ?>
                        <h4>Mailing Address</h4>
                        <p class="address"><?= $address.'<br>'.$city.' , '.$province. '<br>'.$postal?></p>
                        <?php
                    }
                    ?>
                </div>
                <div class="map-holder split-col">
                    <h4>Call</h4>
                    <p class="tel">
                    <?php 
                        if($phone){?>
                        <a href="tel:<?= $phone ?>"><strong>Cell Phone: </strong><?=  $phone ?></a>
                            <?php
                        }
                        if($tollfree){?>
                        <a href="tel:<?=$tollfree; ?>"><strong>Toll Free: </strong><?= $tollfree ?></a>
                        <?php
                        }
                        if($fax){?>
                        <a href="tel:<?= $fax ?>"><strong>Fax </strong><?= $fax ?></a>
                            <?php
                        }
                    ?>
                    </p>
                </div> 
                <div class="map-holder split-col">
                    <h4>Hours</h4>
                    <?php
                        if($departmentonename){?>
                            <h5><?= $departmentonename ?></h5>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel1 && $departmentonetime1){?>
                            <p class="address hours"><span><?= $departmentonelabel1 ?>:</span><span><?= $departmentonetime1 ?></span></p>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel2 && $departmentonetime2){?>
                            <p class="address hours"><span><?= $departmentonelabel2 ?>:</span><span><?= $departmentonetime2 ?></span></p>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel3 && $departmentonetime3){?>
                            <p class="address hours"><span><?= $departmentonelabel3 ?>:</span><span><?= $departmentonetime3 ?></span></p>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel4 && $departmentonetime4){?>
                            <p class="address hours"><span><?= $departmentonelabel4 ?>:</span><span><?= $departmentonetime4 ?></span></p>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel5 && $departmentonetime5){?>
                            <p class="address hours"><span><?= $departmentonelabel5 ?>:</span><span><?= $departmentonetime5 ?></span></p>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel6 && $departmentonetime6){?>
                            <p class="address hours"><span><?= $departmentonelabel6 ?>:</span><span><?= $departmentonetime6 ?></span></p>
                            <?php
                        }
                    ?>
                    <?php
                        if($departmentonelabel7 && $departmentonetime7){?>
                            <p class="address hours"><span><?= $departmentonelabel7 ?>:</span><span><?= $departmentonetime7 ?></span></p>
                            <?php
                        }
                    ?>
                </div>
                <?php
                    if($departmenttwoname){?>
                        <div class="map-holder split-col">
                            <h4><?=$departmenttwoname?> Hours</h4>
                            <?php
                                if($departmenttwolabel1 && $departmenttwotime1){?>
                                    <p class="address hours"><span><?= $departmenttwolabel1 ?>:</span><span><?= $departmenttwotime1 ?></span></p>
                                    <?php
                                }
                            ?>
                            <?php
                                if($departmenttwolabel2 && $departmenttwotime2){?>
                                    <p class="address hours"><span><?= $departmenttwolabel2 ?>:</span><span><?= $departmenttwotime2 ?></span></p>
                                    <?php
                                }
                            ?>
                            <?php
                                if($departmenttwolabel3 && $departmenttwotime3){?>
                                    <p class="address hours"><span><?= $departmenttwolabel3 ?>:</span><span><?= $departmenttwotime3 ?></span></p>
                                    <?php
                                }
                            ?>
                            <?php
                                if($departmenttwolabel4 && $departmenttwotime4){?>
                                    <p class="address hours"><span><?= $departmenttwolabel4 ?>:</span><span><?= $departmenttwotime4 ?></span></p>
                                    <?php
                                }
                            ?>
                            <?php
                                if($departmenttwolabel5 && $departmenttwotime5){?>
                                    <p class="address hours"><span><?= $departmenttwolabel5 ?>:</span><span><?= $departmenttwotime5 ?></span></p>
                                    <?php
                                }
                            ?>
                            <?php
                                if($departmenttwolabel6 && $departmenttwotime6){?>
                                    <p class="address hours"><span><?= $departmenttwolabel6 ?>:</span><span><?= $departmenttwotime6 ?></span></p>
                                    <?php
                                }
                            ?>
                            <?php
                                if($departmenttwolabel7 && $departmenttwotime7){?>
                                    <p class="address hours"><span><?= $departmenttwolabel7 ?>:</span><span><?= $departmenttwotime7 ?></span></p>
                                    <?php
                                }
                            ?>
                        </div>
                        <?php 
                    }
                ?>
                <?php
                    if($email){?>
                        <div class="map-holder split-col">
                            <h4>Email</h4>
                            <p class="tel"><a style="padding-top:0;" href="mailto:<?= $email ?>"><?= $email ?></a></p>
                        </div>
                        <?php 
                    }
                ?>
                <?php
                    if($holiday){?>
                        <div class="map-holder split-100">
                            <h4>Holiday Hours</h4>
                            <p class="address hours"><?=$holiday?></p>
                        </div>
                        <?php 
                    }
                ?>
                </div>
            </div>
        </div>
    </div><!--/.split-content-->

</div><!--/.section-split-->
