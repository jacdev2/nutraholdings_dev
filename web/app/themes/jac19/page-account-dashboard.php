<?php //get_template_part('templates/account-nav') ?>
<div class="woocommerce-MyAccount-content section">
    <p><?php
        /* translators: 1: user display name 2: logout url */
        printf(
            __( 'Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce' ),
            '<strong>' . esc_html( $current_user->display_name ) . '</strong>',
            esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
        );
        ?></p>

    <p><?php
        printf(
            __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' ),
            esc_url( get_bloginfo('url').'/account/orders' ),
            esc_url( get_bloginfo('url').'/account/edit-address' ),
            esc_url( get_bloginfo('url').'/account/edit-account' )
        );
        ?></p>
</div>
<?php 
    if(is_user_logged_in()){ ?>
        <div class="tile-wrap tile-wrap-dashboard section">
            <div class="tile tile-dashboard">
                <div class="inner">
                    <i class="far fa-truck"></i>
                    <h3>Orders</h3>
                    <p>View past order details & status.</p>
                </div><!-- inner -->
                <div class="tile-buttons inner">
                    <a href="<?= bloginfo('url') ?>/account/orders" title="View your orders" class="button primary">View</a>
                </div>
            </div>
            <div class="tile tile-dashboard">
                <div class="inner">
                    <i class="far fa-location-arrow"></i>
                    <h3>Addresses</h3>
                    <p>View/edit your billing & shipping addresses.</p>
                </div><!-- inner -->
                <div class="tile-buttons inner">
                    <a href="<?= bloginfo('url') ?>/account/edit-address" title="View/edit your addresses" class="button primary">View</a>
                </div><!-- tile-buttons -->
            </div>
            <div class="tile tile-dashboard">
                <div class="inner">
                    <i class="far fa-address-card"></i>
                    <h3>Account Details</h3>
                    <p>Change your name, username & password.</p>
                </div><!-- inner -->
                <div class='tile-buttons inner'>
                    <a href="<?= bloginfo('url') ?>/account/edit-account" title="View your account details" class="button primary">View</a>
                </div><!-- tile-buttons -->
            </div>
        </div><!-- tile-wrap -->
        <?php 
    }else{} 
?>
<div class="section-bottom"><?php get_template_part('woocommerce/myaccount/form-login');?></div>
<?php get_template_part('templates/account-banner');?>


