(function($) {

  $( document ).ready(function() {

    if ($('#image-gallery.postbox').length) { // check if gallery meta box exists

      var jac_gallery_frame, images;

      $(document)

        .on('click', '.js-gallery-add-images', function (event) {

          var _this = $(this);

          event.preventDefault();

          // If the media frame already exists, reopen it.
          if ( jac_gallery_frame ) {
            jac_gallery_frame.open();
            return;
          }

          // Create a new media frame
          jac_gallery_frame = wp.media({
            title: 'Upload or select images (hold shift/ctrl key for multi images)',
            library : {
              type : 'image'
            },
            button: {
              text: 'Add images'
            },
            multiple: true  // Set to true to allow multiple files to be selected
          });

          // When an image is selected in the media frame...
          jac_gallery_frame.on( 'select', function() {

            // Get media attachment details from the frame state
            images = jac_gallery_frame.state().get('selection').toJSON();

            showLoader();

            $.ajax({
              type: 'post',
              dataType: 'html',
              url: ajaxurl,
              data: {
                action: 'jac_gallery_select_images',
                post: _this.data('post'),
                images: images
              },
              success: function (r) {
                hideLoader();
                $('.js-gallery-images').append(r);
              }
            });

          });

          // Finally, open the modal on click
          jac_gallery_frame.open();

        })

        .on('click', '.js-gallery-delete-image', function (e) {

          e.preventDefault();

          var _this = $(this);

          if (_this.closest('.js-gallery-image').hasClass('js-unsaved')) {
            _this.closest('.js-gallery-image').hide('slow', function () {
              $(this).remove();
            });
          } else {
            showLoader();
            $.ajax({
              type: 'post',
              dataType: 'json',
              url: ajaxurl,
              data: {
                action: 'jac_gallery_delete_image',
                post: _this.data('post'),
                image: _this.data('image')
              },
              success: function (r) {
                hideLoader();
                if (r.success) {
                  _this.closest('.js-gallery-image').hide('slow', function () {
                    $(this).remove();
                  });
                } else {
                  alert('Error! Please refresh and try again.');
                }
              }
            });
          }

        })

      ; // $(document)

    } // close for check gallery meta box exists

  });

})(jQuery);
