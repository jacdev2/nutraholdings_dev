(function($) {
  if ($(".js-menu-order-quick-change").length) {
    $(document).on("change", ".js-menu-order-quick-change", function() {
      var _this = $(this);

      $.ajax({
        type: "get",
        url: ajaxurl,
        dataType: "json",
        data: {
          action: "menu_order_quick_change",
          order: _this.val(),
          id: _this.data("id")
        },
        success: function(r) {
          if (r.success) {
            _this.addClass("success").removeClass("error");
          } else {
            _this.addClass("error").removeClass("success");
          }
        }
      });
    });
  }

  if ($("#js-exp-date").length) {
    $("#js-exp-date").datepicker({ dateFormat: "yy-mm-dd" });
  }

  myMediaUploader($(".js-doc-uploader"), false, "cf[_doc]");
})(jQuery);
