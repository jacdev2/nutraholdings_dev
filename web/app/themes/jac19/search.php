
<div class="section-grid">
  <?php get_template_part('templates/pagination-search'); ?>
  <div class="section container">
    <?php if (!have_posts()) : ?>
      <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'sage'); ?>
      </div>
      <?php get_search_form(); ?>
    <?php endif; ?>

    <div class="tile-wrap tile-wrap-<?= get_post_type() ?>">
      <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/content', 'search'); ?>
      <?php endwhile; ?>
      <?php
        if (  $wp_query->max_num_pages > 1 ){
            echo '<a class="js-loadmore-posts ajax-button-container section-top"><span class="button primary">View More</span></a>';
        }
      ?>
    </div><!--/.tile-wrap-->
  </div><!--/.container-->
</div><!--/.section-grid-->
<?php the_posts_navigation(); ?>
