<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <?php get_template_part('templates/loader'); ?>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php get_template_part('templates/menu-fixed'); ?>
    <div class="wrapper">
      <?php
        do_action('get_header');
        // override $post variable to enable page content on archives
        get_template_part('templates/post-conditions');
        setup_postdata($post);
        if( is_search() || is_404() || !get_post_type() ) {
          get_template_part('templates/header','simple');
        } else {
          get_template_part('templates/header');
        }
        wp_reset_postdata();
      ?>
      <div class="wrap section content" role="document">
        <div class="container">
          <main class="main">
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->
        </div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php
        get_template_part('templates/post-conditions');
        setup_postdata($post);
        if (!Setup\display_sidebar()) : get_template_part('templates/cta'); endif;
        wp_reset_postdata();

        do_action('get_footer');
        get_template_part('templates/footer');
      ?>
    </div><!-- /.wrapper -->
    <?php get_template_part('templates/menu-off-canvas'); ?>
    <?php
      wp_footer();
    ?>
  </body>
</html>
