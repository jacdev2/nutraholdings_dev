<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
<div class="section-sitemap">
  <?php

    $args = [ 'public' => true, ];
    $postTypes = get_post_types( $args, 'objects' );
    $rewrites = [];
    $types = [];


    foreach($postTypes as $postType) {
      // echo '<pre>'.print_r ($postType,true).'</pre>';
      array_push($rewrites, $postType->rewrite['slug']);
      array_push($types, $postType->name);
    }

    // echo '<pre>'.print_r ($rewrites,true).'</pre>';
    // echo '<pre>'.print_r ($types,true).'</pre>';

    // $sitemap = get_page_by_path('sitemap');
    $pages = get_posts([
      'posts_per_page' => -1,
      'post_type' => 'page',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'post_parent' => 0,
      'post__not_in' => [get_the_ID()],
    ]);
    foreach ($pages as $item) {
      $descendants = get_posts([
        'posts_per_page' => -1,
        'post_type' => 'page',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_parent' => $item->ID,
      ]);
  ?>

  <?php if( $item->menu_order > 0) { ?>
  <ul<?php if($descendants || in_array($item->post_name, $rewrites)) { echo ' class="sitemap-descendants"'; } ?>>
    <li class="sitemap-<?= $item->post_name ?>">
      <a href="<?= get_permalink($item) ?>" class="inline"><?= $item->post_title ?></a>
    </li>
    <?php

      if($descendants) { ?>
      <li><ul>
        <?php foreach ($descendants as $sub) { ?>
          <li class="menu-<?= $sub->post_name ?>">
            <a href="<?php echo get_permalink($sub); ?>" class="inline"><?= $sub->post_title; ?></a>
          </li>
        <?php } ?>
      </ul></li>
      <?php } elseif(in_array($item->post_name, $rewrites)) {
        $key = array_search($item->post_name, $rewrites);
        $type = $types[$key];

        if($type == 'post') {
          $orderby = 'date';
          $order = 'DSC';
        } else {
          $orderby = 'menu_order';
          $order = 'ASC';
        }

        $cpts = get_posts([
          'posts_per_page' => -1,
          'post_type' => $type,
          'orderby' => $orderby,
          'order' => $order,
          'post_parent' => 0,
        ]);
        if($cpts) { ?>
          <li><ul>
          <?php foreach ($cpts as $cpt) { ?>
            <li class="menu-<?= $cpt->post_name ?>">
              <a href="<?php echo get_permalink($cpt); ?>" class="inline"><?= $cpt->post_title; ?></a>
            </li>
          <?php } // end foreach  ?>
          </ul></li>
        <?php } // end if $cpts
      } // elseif in array rewrite ?>
    </ul>
    <?php } // if menu_order > 0 ?>
  <?php } ?>
</div><!--/.section-sitemap-->
<?php endwhile; ?>
