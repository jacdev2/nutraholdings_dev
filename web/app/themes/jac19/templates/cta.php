<?php
  //two query variables so we can set a condition based on user selection
  // global display of CTAs that are featured
  // gloabl selection overridden by post-specific selection

if (isset($post)) $cta_id = get_post_meta($post->ID, '_cta', true);
else $cta_id = 0;

$globalCTA = new WP_Query([
  'post_type'   => 'call_to_action',
  'post_status' => 'publish',
  'orderby'   => 'rand',
  'order'     => 'DESC',
  'posts_per_page' => 1,
  'meta_query' => [
    [
      'key' => '_featured',
      'value' => 1
    ]
  ],
]);

if( $cta_id == 'none' ) {

  echo "<!-- Call to Action is disabled on this ".get_post_type().".-->";

} elseif( $cta_id > 0 ) {

  $post = get_post($cta_id);
  setup_postdata($post);
  get_template_part('templates/content',get_post_type());
  wp_reset_postdata();

} elseif( $globalCTA->have_posts() ) {

  while ($globalCTA->have_posts()) {
    $globalCTA->the_post();
    get_template_part('templates/content',get_post_type());
  }

}
wp_reset_postdata();

?>
