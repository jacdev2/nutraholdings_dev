<?php
/*
The below code is just an example. Modify the markup as needed.
Change the "js-menu-open" class to "js-menu-open-left" if you
want off-canvas to open from the left. Don't forget to add
class "left" to nav.menu-off-canvas as well.
*/
?>
<nav class="menu-fixed">
  <?php
        $args = [
            'post_type' => 'notification_bar',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => 5,
        ];

        $query = new WP_Query($args);
        $totalpagescount = new WP_Query([
            'posts_per_page' => -1,
            'post_type'      => 'page',
            'orderby'        => 'menu_order',
            'order'          => 'ASC',
            'post_status' => 'publish',
            'post_parent' => 0
        ]); 
        $totalparentcount = $totalpagescount->post_count;

        if ( $query->have_posts() ) {
            $total = $query->post_count; ?>
            <div <?php if($total > 1) { echo ' class="notification-bar js-slick-notifications"'; } ?>>
                <?php
                $counter = 1;
                while ($query->have_posts()) {
                    $query->the_post();?>    
                    <?php
                        $metaClone = [];
                        for($i=1; $i<=$totalparentcount; $i++){
                            $meta = get_post_meta(get_the_ID(),'management_team-'.$i);
                            // echo $meta[0];
                            $metaClone[$i] = $meta[0];
                        }
                        global $wp_query;
                        if (in_array($wp_query->post->ID, $metaClone)){ 
                            get_template_part('templates/content-notification');
                        } else{} 
                    ?>
                    <?php
                    // get_template_part('templates/content', get_post_type());
                } // while have posts
                ?>
            </div><!--/.js-slick-slides -->
            <?php
        } // if have posts
        wp_reset_postdata();
  ?>
  <div class="topbar bg-gray-dark">
    <div class="container flex-container">
      <div class="menu-other">
        <?php
          if (has_nav_menu('secondary_navigation')) :
            wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'menu-secondary nolist']);
          endif;
        ?>
        <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
      </div>
    </div><!-- container -->
  </div><!-- topbar -->

  <div class="bottombar bg-gray knockout">
    <div class="container flex-container">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
        <img src="<?= roots\sage\assets\asset_path('images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>"/>
      </a>

      <ul class="menu-primary">
        <?php
          $ID_page = get_the_ID();
          if (has_nav_menu('primary_navigation')) {
            $nav_items = wp_get_nav_menu_items('primary');
            foreach ($nav_items as $nav_item){?>
                <li <?php if($nav_item->object_id == $ID_page){?>class="active" <?php }{}?>>
                    <a href="<?php echo $nav_item->url ?>" class="toptitle"> <?php echo $nav_item->title; ?> </a>
                        <?php
                            $ID =  $nav_item->object_id;
                           $pages = get_pages('child_of=' . $ID);
  
                            if (count($pages) > 0){?>
                                <div class="container-dropdown">
                                    <div class="flexbox">
                                        <div class="flexone">
                                            <h3><?php echo $nav_item->title; ?> </h3>
                                        </div>
                                        <div class="flextwo">
                                            <?php
                                                $ID =  $nav_item->object_id;
                                                get_child_links($ID);
                                            ?>
                                        </div>
                                    </div>
                                </div><!-- -submenu -->
                            <?php } else {}
                        ?>
                </li>
              <?php
            }
          }
        ?>
      </ul>
      <div class="menu-right">
        <ul class="menu-side nolist">
         <span class="menu-search js-popup-search"><i class="fa fa-search"></i></span>
          <li class="menu-open bar-container">
            <span><i></i><i></i><i></i></span>
          </li>
        </ul>
      </div>
      <ul class="menu-utility nolist">
        <li class="menu-open js-menu-open">
          <span><i></i><i></i><i></i></span>
        </li>
      </ul>

    </div><!-- container -->
  </div><!-- bottom-bar -->

  <div class="holder-dropdown container">
    <div class="dropdown-menu">
      <a class="dropdown-brand brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
         <img src="<?= roots\sage\assets\asset_path('images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>"/>
      </a>
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu-primary nolist']);
        endif;
        if (has_nav_menu('secondary_navigation')) :
          wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'menu-secondary nolist']);
        endif;
        JAC\get\social();
      ?>
      <div class="contact-dropdown">
        <?php 
          $address = get_option('company_info_address2');
          $city = get_option('company_info_city');
          $country = get_option('company_info_country');
          $province = get_option('company_info_province');
          $phone = get_option('company_info_province');
        ?>
        <?php 
          if($phone){?>
            <p class="call-us"> CALL <?= $phone ?></p>
          <?php }
          if($address){?>
            <p class="other"><?= $address ?></p>
          <?php }
          if($city && $country){?>
            <p class="other"><?= $city ?>, <?= $province ?>, <?= $country ?></p>
          <?php }
        ?>
      </div>
    </div>
  </div>
</nav><!--/.sticky-nav-->

<div id="myModal" class="modal">
    <?php
        $args = [
            'post_type' => 'video_popup',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => 5,
        ];

        $query = new WP_Query($args);
        $totalpagescount = new WP_Query([
            'posts_per_page' => -1,
            'post_type'      => 'page',
            'orderby'        => 'menu_order',
            'order'          => 'ASC',
            'post_status' => 'publish',
            'post_parent' => 0
        ]); 
        $totalparentcount = $totalpagescount->post_count;
        if ( $query->have_posts() ) {
            $total = $query->post_count; ?>
            <div <?php if($total > 1) {?> class="holder-video modal-content js-slick-notifications" <?php } else{?> class="holder-video modal-content"  <?php }?>>
                <?php
                $countervideo = 1;
                while ($query->have_posts()) {
                    $query->the_post();?>    
                    <?php
                        $metaClonevideo = [];
                        for($i=1; $i<=$totalparentcount; $i++){
                            $meta = get_post_meta(get_the_ID(),'video-'.$i);
                            // echo $meta[0];
                            $metaClonevideo[$i] = $meta[0];
                        }
                        global $wp_query;
                        if (in_array($wp_query->post->ID, $metaClonevideo)){
                            get_template_part('templates/content-video-popup');
                        } else{} 
                    ?>
                    <?php
                    // get_template_part('templates/content', get_post_type());
                } // while have posts
                ?>
            </div><!--/.js-slick-slides -->
            <?php
        } // if have posts
        wp_reset_postdata();
    ?>
    <a class="primary button close">CLOSE <i class="fas fa-times"></i></a>
</div>
