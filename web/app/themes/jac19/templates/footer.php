<footer class="content-info knockout">
  <!-- <div class="overlay" style="background-image: url(<?php //echo roots\sage\assets\asset_path('images/image-footer.png'); ?>" alt="<?php bloginfo('name'); ?>);"></div> -->
  <!-- 
  <div class="footer-top bg-gray-footer">
      <div class="flex-container section-split container">
        <div class="logo-container">
          <a class="brand-container" href="<?php //echo esc_url(home_url('/')); ?>" title="<?php //bloginfo('name'); ?>">
            <img src="<?php //echo roots\sage\assets\asset_path('images/logo.png') ?>"/>
          </a>
          <?php //JAC\Get\social(); ?>
        </div>
        <div class="nav-mid">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(['theme_location' => 'tertiary_navigation', 'menu_class' => 'menu-primary nolist']);
            endif;
          ?>
        </div>
        <div class="nav-container">
          <?php //echo do_shortcode( '[gravityform id="1" title="false" description="true"]')?>
        </div>
      </div>
  </div> -->
  <div class="footer-credits">
    <div class="container">
      <?php
        JAC\Get\copyright();
      ?>
      <div class="copyright feedback">
        <?php 
          JAC\Get\privacy();
        ?>
      </div>
      <?php
        JAC\Get\credit();
      ?>
    </div><!--/.container-->
  </div><!--/.row-->
</footer>
