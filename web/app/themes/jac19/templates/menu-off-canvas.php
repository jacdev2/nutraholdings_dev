<?php
  /*
  Off-Canvas Menu opens on the right side by default.
  To change the side off-canvas side to left we need two things:
  - add the class "left" to menu-off-canvas nav container
  - change the "js-menu-open" class to "js-menu-open-left"
  */
?>

<nav class="menu-off-canvas bg-gray-dark knockout">
  <div class="container">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
        <img src="<?= roots\sage\assets\asset_path('images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>"/>
      </a>

      <?php
      // if (has_nav_menu('primary_navigation')) :
      //   wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu-flyout nolist']);
      // endif;
      ?>
      <ul class ="menu-primary">
        <?php
            $post_types = array(
              '15'	=> array('events','conferences')
            );
            $navs = array();
            $nav_items = wp_get_nav_menu_items('primary');
            foreach ($nav_items as $nav_item){
                $ID =  $nav_item->object_id;
                array_push($navs,$ID);
            }
            $x = 1;
            foreach ($navs as $n) :
              $nav = get_post($n);
          ?>
          <div>
          <li class="<?php echo $nav->post_title; ?> has-sub">
              <a href="<?php echo get_permalink($nav); ?>" class="title"><?php echo $nav->post_title; ?></a>
              <?php 
                $querypage = new WP_Query([
                  'post_type' => 'page',
                  'post_status' => 'publish',
                  'orderby' => 'menu_order',
                  'order' => 'ASC',
                  'posts_per_page' => 10,
                  'post_parent' => $n
                ]);
                //For post types, select the post and customize.
                if ($n == 39){?>
                  <span class="sub-arrow drop-arrow"><i class="fas fa-angle-right"  style="font-style: normal; font-family: FontAwesome;"></i></span>
                  <?php
                      $args = array(
                        'posts_per_page'	=> -1,
                        'post_type'			=> 'post',
                        'orderby'			=> 'menu_order',
                        'order'				=> 'ASC'
                      );
                      $drop = get_posts($args);
                  ?>
                  <ul class="sub-pages other">
                    <?php
                      $query = new WP_Query([
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'orderby' => 'menu_order',
                        'order' => 'ASC',
                        'posts_per_page' => 10,
                      ]);
                      if ($query->have_posts()) {
                        while ($query->have_posts()) {
                          global $post;
                          $query->the_post();
                        ?>
                        <li class="dropdown-link">
                          <a href="<?= get_permalink($post); ?>">
                            <i class="fas fa-minus"  style="font-style: normal; font-family: FontAwesome;"></i>
                            <?=
                              the_title();
                            ?>
                          </a> 
                        </li>
                        <?php 
                      } wp_reset_postdata();
                      }?>
                  </ul> <!-- sub-pages-->
                  <?php
                }
                else{  
                  if ($querypage->have_posts()) {?>
                    <span class="sub-arrow drop-arrow"><i class="fas fa-angle-right" style="font-style: normal; font-family: FontAwesome;"></i></span>
                    <?php
                  } ?>
                  <ul class="sub-pages other">
                    <?php
                      if ($querypage->have_posts()) {
                        while ($querypage->have_posts()) {
                          global $post;
                          $querypage->the_post();
                        ?>
                        <li class="dropdown-link">
                          <a href="<?= get_permalink($post); ?>">
                            <i class="fas fa-minus" style="font-family: FontAwesome;"></i>
                            <?=
                              the_title();
                            ?>
                          </a> 
                        </li>
                        <?php 
                      } wp_reset_postdata();
                      }?>
                  </ul> <!-- sub-pages-->
                  <?php 
                }
              ?>
              <?php $x++ ?>
          </li>
          </div>
          <?php endforeach; ?>
      </ul>

      <?php
      if (has_nav_menu('secondary_navigation')) :
        wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'menu-secondary nolist']);
      endif;
      
      JAC\Get\social();
    ?>

  </div><!--/.container-->
</nav><!--/.off-canvas-nav-->
