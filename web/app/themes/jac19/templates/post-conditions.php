<?php
global $post;
global $jac_post_overrides;
// override $post for non page/post templates

if (is_home() || is_archive() || is_category() || is_tag() || is_date() ) {
  $post = get_page_by_path('the-latest/news');
} else {
  $post = get_post();
}

// defined in lib/globals.php
foreach ($jac_post_overrides as $rule) {

  if(isset($rule[0])) { $path = $rule[0]; } else { $path = null; }
  if(isset($rule[1])) { $type = $rule[1]; } else { $type = null; }
  if(isset($rule[2])) { $tax  = $rule[2]; } else { $tax  = null; }
  if(isset($rule[3])) { $hide = $rule[3]; } else { $hide = true; }

  if( ($type && is_post_type_archive($type)) || ($tax && is_tax($tax)) ) {
    $post_override = get_page_by_path($path);

    if(!empty($post_override)) {
      $post = $post_override;
    }
  }

} // foreach $condition
