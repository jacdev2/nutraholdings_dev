<?php
  $classes = [ get_post_type() ];

  $doc = get_post_meta(get_the_ID(), '_doc', true);
  $linkage_text = get_post_meta(get_the_ID(), '_linkage_text', true);
  $linkage_link = get_post_meta(get_the_ID(), '_linkage_link', true);
  $linkage_option = get_post_meta(get_the_ID(), '_linkage_option', true);

  $feat = get_post_thumbnail_id();

  // Button Text. Force "Download" when Document is uploaded.
  if($doc) {
    $button_text = "Download";
  } elseif($linkage_text) {
    $button_text = $linkage_text;
  } else {
    $button_text = 'Read More';
  }

  // Link URL. Uploaded document overrides linkage_url
  if($doc) {
    $linkage_url = wp_get_attachment_url($doc);
  } elseif($linkage_link) {
    $linkage_url = $linkage_link;
  } else {
    $linkage_url = null;
  }

  // Open in new tab if document uploader or new window selected
  if($doc) {
    $target = ' target="_blank"';
  } elseif($linkage_option == 'new') {
    $target = ' target="_blank"';
  } else {
    $target = null;
  }

  // Popup if popup selected and document is empty
  if($doc) {
    $popup = null;
  } elseif($linkage_option == 'popup') {
    $popup = ' js-popup';
  } else {
    $popup = null;
  }
?>

<article <?php post_class($classes); ?> >
  
  <div class="banner-image bg-black" style="background-image: url(<?= wp_get_attachment_image_src( $feat, 'full')[0] ?>);">
  </div>

  <div class="section overlay">
    <div class="blurb">

      <?php
        the_title('<h2 class="act-like-h1">','</h2>');
        the_subtitle('<p class="subtitle">','</p>');
      ?>

      <a
        href="<?= $linkage_url ?>"<?= $target ?>
        class="button primary<?= $popup ?>"
      >
        <?= $button_text ?>
      </a>
    </div><!--/.blurb-->
  </div><!--/.section-overlay-->

</article>
