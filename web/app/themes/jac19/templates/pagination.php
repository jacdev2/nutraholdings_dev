<div class="pagination-wrap">
  <div class="container">
    <nav class="pagination">

      <div class="filter-options js-filter-expand">
        <p>
          <?php if( !is_search() ) { ?>      <a><i class="fa fa-plus-circle"></i> Filter Options</a>
          <?php } ?>
          Page <?= get_query_var('paged') ?: 1 ?> of <?= $wp_query->max_num_pages ?: 1 ?>
        </p>
      </div><!--/.filter-options-->

      <div class="filter-expand js-filter-expander">

        <?php

        global $wp_query;
        global $jac_post_overrides;

        // posts are not in the below array because
        // it is not a custom post type and the functions are different

        foreach ($jac_post_overrides as $rule) {

          if (isset($rule[0])) { $path = $rule[0]; } else { $path = null; } // path/to/page
          if (isset($rule[1])) { $type = $rule[1]; } else { $type = null; } // post_type
          if (isset($rule[2])) { $tax  = $rule[2]; } else { $tax  = null; } // taxonomy
          if (isset($rule[3])) { $hide = $rule[3]; } else { $hide = true; } // hide_empty

          if (($type && is_post_type_archive($type)) || ($tax && is_tax($tax))) { // check where am I

            if ($tax) {
              $terms = get_terms(['taxonomy' => $tax, 'hide_empty' => $hide]);
              $current = get_query_var($tax);
            } else {
              $terms = [];
              $current = '';
            }

            break;

          } // if archive or tax
        } // end foreach $rule

        if (is_home() || is_category() || is_tag() || is_date()) {
          $terms = get_categories();
          $current = get_query_var('category_name');
          $path = 'news';
        }

        if (isset($terms) && !empty($terms)) { ?>
          <div class="select-wrap">
            <select class="js-select2-search js-selector-with-url">
              <option
                value="<?= get_permalink(get_id_by_path($path)) ?>"
                <?php if(is_post_type_archive($type)) { echo "selected"; } ?>
              >
                All
              </option>
        <?php foreach ($terms as $term) { ?>
              <option
                value="<?= get_term_link($term); ?>"
                <?php if ($term->slug == $current) echo ' selected'; ?>
              >
                <?= $term->name ?>
              </option>
        <?php } ?>
            </select>
          </div><!--/.select-wrap-->
        <?php } // endif $terms ?>

        <div class="search-wrap spotlight-search">
          <form role="search" method="get" class="search-form">
            <input type="search" class="search-field" placeholder="Search &hellip;" value="<?= get_query_var('search') ?>" name="search" />
            <button type="submit" class="search-submit" value="Search" ><i class="fa fa-search"></i></button>
          </form>
        </div>
      </div><!--/.filter-expand-->

      <div class="arrows">
        <?php
          if(get_previous_posts_link()) {
            previous_posts_link( '<i class="fa fa-angle-left"></i>' );
          } else {
            echo '<a class="disabled"><i class="fa fa-angle-left"></i></a>';
          }

          if(get_next_posts_link()) {
            next_posts_link( '<i class="fa fa-angle-right"></i>' );
          } else {
            echo '<a class="disabled"><i class="fa fa-angle-right"></i></a>';
          }
        ?>
      </div><!--/.arrows-->
    </nav><!--/.pagination-->
  </div><!--/.container-->
</div><!--/.pagination-wrap-->
