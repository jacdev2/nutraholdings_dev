<?php
  $feat = get_post_thumbnail_id();
  $slug = $post->post_name;
  $doc = get_post_meta(get_the_ID(), '_doc', true);
  $linkage_text = get_post_meta(get_the_ID(), '_linkage_text', true);
  $linkage_link = get_post_meta(get_the_ID(), '_linkage_link', true);
  $linkage_option = get_post_meta(get_the_ID(), '_linkage_option', true);

  $bgcolor = get_post_meta(get_the_ID(), '_color_controls_background', true);
  $foreground = get_post_meta(get_the_ID(), '_color_controls_text', true);

  if(!$bgcolor) {
    $bgcolor = 'white';
  }

  // Button Text. Force "Download" when Document is uploaded.
  if($doc) {
    $button_text = "Download";
  } elseif($linkage_text) {
    $button_text = $linkage_text;
  } else {
    $button_text = 'Read More';
  }

  // Link URL. Uploaded document overrides linkage_url
  if($doc) {
    $linkage_url = wp_get_attachment_url($doc);
  } elseif($linkage_link) {
    $linkage_url = $linkage_link;
  } elseif(is_post_type('application')) {
    $linkage_url = null;
  } else {
    $linkage_url = get_the_permalink();
  }

  // Open in new tab if document uploader or new window selected
  if($doc) {
    $target = ' target="_blank"';
  } elseif($linkage_option == 'new') {
    $target = ' target="_blank"';
  } else {
    $target = null;
  }

  // Popup if popup selected and document is empty
  if($doc) {
    $popup = null;
  } elseif($linkage_option == 'popup') {
    $popup = ' class="js-popup"';
  } else {
    $popup = null;
  }

  $classes = ['section','tile-cta', 'cta-'.$slug, 'bg-'.$bgcolor, $foreground ];
?>

<article <?php post_class($classes); ?>>
  <div class="blurb">
    <?php
      the_title('<h2>','</h2>');
      the_excerpt();
    ?>
    <?php if($linkage_url) { ?>
      <a href="<?= $linkage_url ?>" class="button inverted"<?= $target ?>><?= $button_text ?></a>
    <?php } ?>
  </div><!--/.tile-content-->
  <?php if(!empty($feat)) { ?>
   <div class="tile-bg" style="background-image: url(<?= wp_get_attachment_image_src( $feat, 'hero-lg')[0] ?>);"></div>
  <?php } ?>
</article>
