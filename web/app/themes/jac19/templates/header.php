<?php if(is_front_page()) { ?>
  <?php
    $args = [
      'post_type' => 'slide',
      'post_status' => 'publish',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'posts_per_page' => 5,
    ];

    $query = new WP_Query($args);
     $totalpagescount = new WP_Query([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
        'post_status' => 'publish',
        'post_parent' => 0
    ]); 
    $totalparentcount = $totalpagescount->post_count;

    if ( $query->have_posts() ) {
      $total = $query->post_count;?>
     <header class="banner knockout bg-gray-dark">
            <div <?php if($total > 1) { echo ' class="js-slick-heroes"'; } ?>>
            <?php
              $counter = 1;
              while ($query->have_posts()) {
                $query->the_post();?>    
                  <?php
                    $metaClone = [];
                    for($i=1; $i<=$totalparentcount; $i++){
                      $meta = get_post_meta(get_the_ID(),'management_team-'.$i);
                      echo $meta[0];
                      $metaClone[$i] = $meta[0];
                    }
                    global $wp_query;
                    if (in_array($wp_query->post->ID, $metaClone)){ 
                       get_template_part('templates/content', get_post_type());
                    } else{} 
                  ?>
                <?php
                // get_template_part('templates/content', get_post_type());
              } // while have posts
            ?>
            </div><!--/.js-slick-slides -->
          </header><!--/.banner-->
    <?php
    } // if have posts
    wp_reset_postdata();
  ?>

<?php } elseif(is_page(['contact'])){}elseif( is_singular(['artist','product']) || is_page(['account-dashboard','privacy-policy','account','cart','checkout']) ) { ?>

  <?php get_template_part('templates/breadcrumbs'); ?>

<?php } elseif(is_page(['sitemap','legal'])){?>
  <?php get_template_part('templates/header-simple'); ?>
<?php } else {
    $args = [
      'post_type' => 'slide',
      'post_status' => 'publish',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'posts_per_page' => -1,
    ];

    $metaClone = [];
    global $wp_query;
    $id_top = $wp_query->post->ID;
    $query = new WP_Query($args);
    $totalpagescount = new WP_Query([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
        'post_status' => 'publish',
        // 'post_parent' => 0
    ]); 
    $totalparentcount = $totalpagescount->post_count;
  
    if ( $query->have_posts() ) {
      $total = $query->post_count;
      $counter = 1;
      while ($query->have_posts()) {
        $query->the_post();
        for($i=1; $i<=$totalparentcount; $i++){
          $meta = get_post_meta(get_the_ID(),'management_team-'.$i);
          array_push( $metaClone, $meta[0]);
          // $metaClone[$i] = $meta[0];
        }
      } // while have posts
    } // if have posts
    wp_reset_postdata();


    if (in_array($id_top, $metaClone)){  
      $metaClonecheck = [];
      if ( $query->have_posts() ) {
        $total = $query->post_count;?>
        <header class="banner knockout bg-white">
          <div <?php if($total > 1) { echo ' class="js-slick-heroes"'; } ?>>
            <?php
              $counter = 1;
              while ($query->have_posts()) {
                $query->the_post();
                for($i=1; $i<=$totalparentcount; $i++){
                  $meta = get_post_meta(get_the_ID(),'management_team-'.$i);
                  $metaClonecheck[$i] = $meta[0];
                }
                if (in_array($id_top, $metaClonecheck)){  
                   get_template_part('templates/content', get_post_type());
                }else{}
              } // while have posts
            ?>
          </div><!--/.js-slick-slides -->
        </header><!--/.banner-->
        <?php
      } // if have posts
      wp_reset_postdata();
    } else{?>
        <header class="banner knockout bg-white">
          <?php
            get_template_part('templates/content-slide-other');
          ?>
        </header>
        <?php
    }     
} ?>
<?php if(is_cart() || is_checkout() || is_wc_endpoint_url( 'order-received' )){ ?>
    <div class="shopping-steps section blurb">
        <a href="<?= bloginfo('url')."/cart" ?>" class="step <?php if(is_cart()){ echo "active"; } ?>" title="go back to cart">
            <span>1</span>
            <p>Shopping Cart</p>
        </a><!-- step -->
        <a href="<?= bloginfo('url')."/checkout" ?>" class="step <?php if(is_checkout() && !is_wc_endpoint_url( 'order-received' )){ echo "active"; } ?>">
            <span>2</span>
            <p>Billing &amp; Checkout</p>
        </a><!-- step -->
        <div class="step <?php if(is_wc_endpoint_url( 'order-received' )){ echo "active"; } ?>">
            <span>3</span>
            <p>Confirmation</p>
        </div><!-- step -->
    </div><!-- shopping-steps -->
<?php } ?>
