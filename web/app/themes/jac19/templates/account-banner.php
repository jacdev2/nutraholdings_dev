<div class="account-banner">
  <div class="tile-banner section-split inner">
    <div class="split-10 content-left"><i class="fa fa-map"></i></div>
    <div class="split-90 content-right">
      <p>
        <strong>Free Canada/US Shipping</strong>
        <br>
        On all orders over $150
      </p>
    </div><!-- split-80 content-right-->
  </div><!-- split-25 -->
  <div class="tile-banner section-split inner">
    <div class="split-10 content-left"><i class="fa fa-undo"></i></div>
    <div class="split-90 content-right">
      <p>
        <strong>Easy 30 Day Returns</strong>
        <br>
        30 day money back guarantee
      </p>
    </div><!-- split-80 content-right-->
  </div><!-- split-25 -->
  <div class="tile-banner section-split inner">
    <div class="split-10 content-left"><i class="fa fa-check"></i></div>
    <div class="split-90 content-right">
      <p>
        <strong>Warranty</strong>
        <br>
        1 year warranty on all products
      </p>
    </div><!-- split-80 content-right-->
  </div><!-- split-25 -->
  <div class="tile-banner section-split inner">
    <div class="split-10 content-left"><i class="fa fa-lock"></i></div>
    <div class="split-90 content-right">
      <p>
        <strong>100% Secure Checkout</strong>
        <br>
        PayPal | Mastercard | Visa | Amex
      </p>
    </div><!-- split-80 content-right-->
  </div><!-- split-25 -->
</div><!-- container -->
