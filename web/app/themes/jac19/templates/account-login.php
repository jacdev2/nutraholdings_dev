<?php 
    $feat = get_post_thumbnail_id(); 
    $secondary = get_post_meta(get_the_ID(), get_post_type() . '_secondary-image_thumbnail_id', true);  
    if($secondary) {
        $imageID = $secondary;
    } else {
    $imageID = $feat;
    }
?>
<div class="container">
    <div class="modal modal-login lead-form">
        <h2 class="login_title">Account Login</h2>
        <form action="<?php the_permalink(); ?>" method="post" class="modal-form login js-jac-account-form" data-action="login">
            <div class="input-wrap form-row form-row-wide">
                <i style="color: white" class="fas fa-user"></i>
                <input id="login-email" type="email" name="email" placeholder="Email"/>
                <!-- <label class="modal-field login-email" for="login-email" style="font-size:12px;">Email</label> -->
            </div>
            <div class="input-wrap form-row form-row-wide">
                <i style="color: white"  class="fas fa-unlock"></i> 
                <input id="login-password" type="password" name="password" placeholder="Password"/>
                <!-- <label class="modal-field login-password"  for="login-password"  style="font-size:12px;">Password</label>-->
            </div>
            <p class="js-success-message js-error-message"></p>
            <div class="form-row flex-box" style="padding-top: 1em;">
                <div class="checkbox-wrap">
                    <input id="login-remember" type="checkbox" name="remember" value="1" checked />
                    <label class="login-remember" for="login-remember">Remember me</label>
                </div><!--/.checkbox-wrap-->
                <div class="flex-one">
                    <button class="secondary" type="submit">Sign in</button>
                </div>
            </div><!--/.flex-box-->

            <p class="login-forgot">
                    <a href="<?php the_permalink(129); ?>?reset">Lost your password?</a>
                </p>
        </form>

        <p class="login-signup">
            Don't have an account?
            <a href="<?php the_permalink(129); ?>?register">Create One</a>
        </p>
    </div><!--/.modal-login-->
</div>
