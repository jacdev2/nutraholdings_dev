<div class="browse-happy bg-primary knockout">
  <div class="blurb inner">
    <p><strong>WARNING:</strong> You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
    <a href="http://browsehappy.com/" class="button primary">Upgrade Now</a>
  </div>
  <a href="#" class="browse-happy-close js-browse-happy-close">x</a>
</div>
