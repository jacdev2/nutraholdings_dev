<?php
  $classes = [ get_post_type() ];

  $ID = $wp_query->post->ID;
  $doc = get_post_meta($ID, '_doc', true);
  $linkage_text = get_post_meta($ID, '_linkage_text', true);
  $linkage_link = get_post_meta($ID, '_linkage_link', true);
  $linkage_option = get_post_meta($ID, '_linkage_option', true);
  $feat = get_post_thumbnail_id($ID);

  // Button Text. Force "Download" when Document is uploaded.
  if($doc) {
    $button_text = "Download";
  } elseif($linkage_text) {
    $button_text = $linkage_text;
  } else {
    $button_text = 'Read More';
  }

  // Link URL. Uploaded document overrides linkage_url
  if($doc) {
    $linkage_url = wp_get_attachment_url($doc);
  } elseif($linkage_link) {
    $linkage_url = $linkage_link;
  } else {
    $linkage_url = null;
  }

  // Open in new tab if document uploader or new window selected
  if($doc) {
    $target = ' target="_blank"';
  } elseif($linkage_option == 'new') {
    $target = ' target="_blank"';
  } else {
    $target = null;
  }

  // Popup if popup selected and document is empty
  if($doc) {
    $popup = null;
  } elseif($linkage_option == 'popup') {
    $popup = ' js-popup';
  } else {
    $popup = null;
  }
?>

<article <?php post_class($classes); ?> >
  
  <div <?php if(!$feat){?> class="hide-for-small-only nofeat banner-image bg-quad" <?php } else{?> class="banner-image bg-quad" <?php } ?> style="background-image: url(<?= wp_get_attachment_image_src( $feat, 'full')[0] ?>);">
  </div>

  <div class="section overlay bg-gray-dark" <?php if(!$feat){?> style="background-color: #000000;"<?php } else{} ?>>
    <div class="blurb">
        <?php 
            if( !get_the_subtitle($ID)){?>
                 <h2 class="act-like-h1"><?=get_the_title($ID)?></h2>
                 <?php
            } else {?>
                 <h2 class="act-like-h1"><?=get_the_subtitle($ID)?></h2> <?php
            } 
        ?>
        <p><?= get_the_excerpt($ID)?></p>

      <?php 
        if($linkage_link){?>
          <a
            href="<?= $linkage_url ?>"<?= $target ?>
            class="button primary<?= $popup ?>"
          >
            <?= $button_text ?>
          </a>
          <?php
        } else {}
      ?>
    </div><!--/.blurb-->
  </div><!--/.section-overlay-->

</article>
