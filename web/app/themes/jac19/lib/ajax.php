<?php
new Ajax();
class Ajax {

  public function __construct() {

    $functions = get_class_methods($this);

    foreach ($functions as $function) {
      if ($function[0] == '_') continue;
      add_action( 'wp_ajax_' . $function, [$this, $function] );
      add_action( 'wp_ajax_nopriv_' . $function, [$this, $function] );
    }

  }

  private function _exit() {
    if (defined('DOING_AJAX') && DOING_AJAX) wp_die();
  }

  public function the_media_gallery_grid($param = []) {

    global $post;
    $per_page = 12;
    $now_page = isset($_GET['paged']) ? $_GET['paged'] : 1;

    if (isset($_GET['postid'])) $post_id = $_GET['postid'];
    else if (isset($param['postid'])) $post_id = $param['postid'];
    else $post_id = $post->ID;

    $videos = \JAC\Video\get_videos($post_id);
    $images = \JAC\Gallery\get_images($post_id);

    $combine = array_merge($videos, $images);

    if (isset($_GET['type'])) {
      switch ($_GET['type']) {
        case 'photo':
          $combine = $images;
          break;
        case 'video':
          $combine = $videos;
          break;
      }
    }

    $index = ($now_page - 1) * $per_page;
    $current = array_slice($combine, $index, $per_page);

    $total = count($combine);
    $pages = ceil( $total / $per_page );
    $data = ' data-pages="' . $pages . '" data-total="' . $total . '"';
    ?>

    <div class="media-gallery-wrap js-total-count"<?php echo $data; ?>>
      <?php foreach ($current as $single) {
        \JAC\Get\media_item($single);
      } ?>
    </div>

    <?php $this->_exit();

  }

   public function the_media_gallery_grid_promotion($param = []) {

    global $post;
    $per_page = 12;
    $now_page = isset($_GET['paged']) ? $_GET['paged'] : 1;

    $post_id = 129;
    
    $videos = \JAC\Video\get_videos($post_id);
    $images = \JAC\Gallery\get_images($post_id);

    $combine = array_merge($videos, $images);

    if (isset($_GET['type'])) {
      switch ($_GET['type']) {
        case 'photo':
          $combine = $images;
          break;
        case 'video':
          $combine = $videos;
          break;
      }
    }

    $index = ($now_page - 1) * $per_page;
    $current = array_slice($combine, $index, $per_page);

    $total = count($combine);
    $pages = ceil( $total / $per_page );
    $data = ' data-pages="' . $pages . '" data-total="' . $total . '"';
    ?>

    <div class="media-gallery-wrap js-total-count"<?php echo $data; ?>>
      <?php foreach ($current as $single) {
        \JAC\Get\media_item($single);
      } ?>
    </div>

    <?php $this->_exit();

  }
}
