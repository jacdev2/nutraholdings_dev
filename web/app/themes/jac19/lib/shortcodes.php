<?php
namespace JAC\Shortcodes;

// [bartag foo="foo-value"]
function shortcode_company_info( $atts ) {
  $a = shortcode_atts( array(
      'field' => 'company',
  ), $atts );

  $field = get_option('company_info_' . $a['field'] );

  if(!empty($field)) {
    return stripslashes($field);
  } else {
    return '<strong style="color: red;">FIELD EMPTY</strong>';
  }
}
add_shortcode( 'company_info', __NAMESPACE__ . '\\shortcode_company_info' );
