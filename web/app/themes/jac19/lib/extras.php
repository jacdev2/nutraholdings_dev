<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;


// Master Theme Configuration
// Comment out the lines that do not apply to the current project.
//add_action( 'wp_footer', 'JAC\Get\popup_feedback' );
add_action( 'wp_footer', 'JAC\Get\popup_search' );
add_theme_support( 'google-api-key' ); //applies ajax media gallery to all pages on the website
add_theme_support( 'media-gallery' ); //applies ajax media gallery to all pages on the website
add_theme_support( 'typekit' ); //generate typekits with frontend@jac.co adobe account

// add_image_srcset( 'hero',     1920, 593, true); // for subpage banners. measure design for height
// add_image_srcset( 'slide',    1920, 733, true); // for home slides. measure the design. remove if the same as hero.
// add_image_srcset( 'article',  640,  400, true, 3); // for article grids. see JAC\Get\article_srcset()
// add_image_size(   'overview', 960,  540, true); // for overview grids where the image is a background image.

if(current_theme_supports('media-gallery')) {
  add_image_size( 'gallery-thumbnail', 320,  240, true); // for the media gallery function
}

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


class Foundation_Nav_Menu extends \Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"menu\">\n";
    }
}
