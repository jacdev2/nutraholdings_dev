<?php

namespace JAC\Gallery;

add_action('add_meta_boxes', function ($post_type, $post) {

  if ( post_type_supports( $post_type, 'media-gallery' ) && current_theme_supports( 'media-gallery' ) ) {
    add_meta_box('image-gallery', 'Image Gallery', __NAMESPACE__ . '\\image_gallery_meta_box', $post_type, 'normal', 'high');
  }

}, 10, 2);

function image_gallery_meta_box($post) {
  ?>
  <div class="jac-metabox meta-gallery-picker js-gallery-images">
    <?php
    $gallery = get_post_meta($post->ID, '_jac_gallery');
//    if (empty($gallery))
//      $gallery = [
//        ['id' => 7, 'order' => 20],
//        ['id' => 8, 'order' => 10],
//      ];

    usort($gallery, function ($a, $b) {
      return $a['order'] - $b['order'];
    });

    foreach ($gallery as $image) {
      display_image($post->ID, $image);
    }
    ?>
  </div>
  <p class="jac-metabox-add hide-if-no-js">
    <a href="<?= esc_url( get_upload_iframe_src( 'image', $post->ID ) ) ?>" data-post="<?= $post->ID ?>" class="js-gallery-add-images"><i class="fa fa-plus-circle"></i> Add Images</a>
  </p>
<?php }

new GalleryAjax();
class GalleryAjax {

  public function __construct() {

    $functions = get_class_methods($this);

    foreach ($functions as $function) {
      if ($function[0] == '_') continue;
      add_action( 'wp_ajax_jac_gallery_' . $function, [$this, $function] );
    }

  }

  private function _exit() {
    if (defined('DOING_AJAX') && DOING_AJAX) wp_die();
  }

  public function select_images() {
    $post_id = isset($_POST['post']) ? $_POST['post'] : null;
    $images = isset($_POST['images']) ? $_POST['images'] : [];
    foreach ($images as $image) {
      display_image($post_id, ['id' => $image['id'], 'order' => 0]);
    }
    $this->_exit();
  }

  public function delete_image() {
    $post_id = isset($_POST['post']) ? $_POST['post'] : null;
    $image_id = isset($_POST['image']) ? $_POST['image'] : null;

    if ($post_id && $image_id) {

      // get gallery from post meta
      $gallery = get_post_meta($post_id, '_jac_gallery');

      // loop the gallery find image and delete it
      foreach ($gallery as $image) {
        if ($image['id'] == $image_id) {
          delete_post_meta($post_id, '_jac_gallery', $image);
          wp_send_json_success();
        }
      }

    }

    wp_send_json_error();
  }

}

function display_image($post_id, $image) {
  $image_post = get_post($image['id']);
  $image_alt = get_post_meta($image_post->ID, '_wp_attachment_image_alt', true);
  ?>
  <ul class="meta-gallery-row js-gallery-image<?php if (defined('DOING_AJAX') && DOING_AJAX) { echo ' js-unsaved';} ?>">
    <li class="meta-gallery-order">
      <input type="hidden" name="js-gallery-image[<?= $image['id'] ?>][id]" value="<?= $image['id'] ?>">
      <p>Order:</p>
      <input class="text" name="js-gallery-image[<?= $image['id'] ?>][order]" value="<?= $image['order'] ?>" placeholder="0">
    </li>
    <li class="meta-gallery-thumbnail">
      <a class="meta-gallery-delete js-gallery-delete-image" data-post="<?= $post_id ?>" data-image="<?= $image['id'] ?>"><span>Remove</span> <i class="fa fa-times-circle"></i></a>
      <a href="<?= wp_get_attachment_image_src($image['id'], 'full')[0]; ?>" data-title="<?= $image_post->post_title; ?>" data-gallery="gallery" class="js-popup">
        <img src="<?= wp_get_attachment_image_src($image['id'])[0]; ?>" alt="<?= $image_alt ?>">
      </a>
    </li>
    <li class="meta-gallery-details">
      <input class="text" name="js-gallery-image[<?= $image['id'] ?>][post_title]" placeholder="Title" value="<?= $image_post->post_title; ?>">
      <input class="text" name="js-gallery-image[<?= $image['id'] ?>][post_excerpt]" placeholder="Caption" value="<?= $image_post->post_excerpt; ?>">
      <input class="text" name="js-gallery-image[<?= $image['id'] ?>][alt]" placeholder="Alt Text" value="<?= $image_alt ?>">
    </li><!--/.meta-gallery-details-->
    <li class="meta-gallery-description">
      <textarea class="text" name="js-gallery-image[<?= $image['id'] ?>][post_content]" placeholder="Description"><?= $image_post->post_content; ?></textarea>
    </li>
  </ul><!--/.js-gallery-image-->
<?php }

add_action('save_post', function ($post_id) {

  $images = isset($_POST['js-gallery-image']) ? $_POST['js-gallery-image'] : null;

  if ($images) {
    delete_post_meta($post_id, '_jac_gallery');

    foreach ($images as $id => $image) {
      add_post_meta($post_id, '_jac_gallery', ['id' => $id, 'order' => (int)$image['order']]); // casting to int
      wp_update_post([
        'ID' => $id,
        'post_title' => $image['post_title'],
        'post_excerpt' => $image['post_excerpt'],
        'post_content' => $image['post_content'],
      ]);
      if (!empty($image['alt'])) update_post_meta($id, '_wp_attachment_image_alt', $image['alt']);
    }
  }

});

// delete gallery meta when delete post
add_action( 'deleted_post', function ($post_id) {
  delete_post_meta($post_id, '_jac_gallery');
});

// get gallery and sort it then return image id array
function get_images($post = 0) {
  if (!empty($post)) {
    $post = get_post( $post );
  } else {
    global $post;
  }

  $gallery = get_post_meta($post->ID, '_jac_gallery');

  usort($gallery, function ($a, $b) {
    return $a['order'] - $b['order'];
  });

  $result = [];

  foreach ($gallery as $image) {
    $result[] = $image['id'];
  }

  return $result;

}
