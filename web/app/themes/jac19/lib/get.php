<?php

namespace JAC\Get;

add_action('wp_head',__NAMESPACE__.'\\favicons');
function favicons() {
  $favicon_path = '/app/themes/jac/dist/favicons/';
?>

<link rel="apple-touch-icon" sizes="180x180" href="<?= bloginfo('url').$favicon_path ?>apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?= bloginfo('url').$favicon_path ?>favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?= bloginfo('url').$favicon_path ?>favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?= bloginfo('url').$favicon_path ?>manifest.json">
<link rel="mask-icon" href="<?= bloginfo('url').$favicon_path ?>safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="<?= bloginfo('url').$favicon_path ?>favicon.ico">
<meta name="msapplication-config" content="<?= bloginfo('url').$favicon_path ?>browserconfig.xml">
<meta name="theme-color" content="<?= get_option('company_info_brand_colour') ?>">

<?php
}

function social( $shape = null, $limit = -1 ) {

$query = new \WP_Query([
    'posts_per_page' => $limit,
    'post_type' => 'social_channel',
    'post_status' => 'publish',
    'orderby' => 'menu_order',
    'order' => 'ASC',
  ]);

  if ($query->have_posts()) {
?>
  <ul class="social<?php if(!empty($shape)) { echo " stacked"; } ?> nolist">
<?php
while ($query->have_posts()) {
  global $post;
  $query->the_post();
  $meta = get_post_custom();

  $icon = get_post_meta(get_the_ID(), '_social_icon', true);
  $handle = get_post_meta(get_the_ID(), '_social_handle', true);
  $url = get_post_meta(get_the_ID(), '_social_url', true);

  if( $icon && $url ) {
?>
  <li>
    <?php if(!empty($shape)) { ?>
    <a
      href="<?= $url ?>"
      class="fa-stack fa-lg"
      target="_blank"
    >
      <i class="fa fa-<?= $shape ?> fa-stack-2x"></i>
      <i class="fa fa-<?= $icon ?> fa-stack-1x"></i>
    </a>
    <?php } else { ?>
    <a
      href="<?= $url ?>"
      <?php if( $handle ) { ?>title="<?= $handle ?>"<?php } ?>
      class="fa fa-<?= $icon ?>"
      target="_blank"
    ></a>
    <?php } ?>
  </li>
<?php
  }
} // while query
?>
  </ul><!--/.social-->
<?php
  } // if have posts
  wp_reset_postdata();

} // end social()

function share_buttons( $id = null ) {

  if(empty( $id )) {
    $id = get_the_ID();
  }

  $post = get_post( $id );

  if(!empty(get_post_thumbnail_id($id))) {
    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'full' );
  } else {
    $image = '';
  }

  if(!empty( get_page_by_path( 'twitter', 'object', 'social_channel' ) )) {
    $twitter = get_page_by_path( 'twitter', 'object', 'social_channel' );
    $twitter_meta = get_post_custom($twitter->ID);
    $twitter_handle = preg_replace( '/[^a-zA-Z0-9_.]/', '', $twitter_meta['_social_handle'][0] );
  } else {
    $twitter_handle = null;
  }

  if(!empty($twitter_handle)) {
    $t_via = '&amp;via='.$twitter_handle;
  } else {
    $t_via = null;
  }

?>
<ul class="rrssb-buttons clearfix">

<?php

  $sharing = array(
    array(
      'Share',
      'share',
      '',
      null,
    ),
    array(
      'Facebook',
      'facebook',
      'https://facebook.com/sharer/sharer.php?u='.urlencode( get_permalink( $post ) ),
      'popup'
    ),
    array(
      'Twitter',
      'twitter',
      'https://twitter.com/intent/tweet?text='.urlencode( $post->post_title).'&amp;url='.urlencode( get_permalink( $post ) ).$t_via,
      'popup'
    ),
    array(
      'Linkedin',
      'linkedin',
      'https://linkedin.com/shareArticle?mini=true&amp;url='.urlencode( get_permalink( $post ) ).'&amp;title='.urlencode( $post->post_title).'&amp;summary='.urlencode( $post->post_excerpt).'&amp;source='.urlencode( get_option('ci_company') ),
      'popup'
    ),
    array(
      'Google+',
      'googleplus',
      'https://plus.google.com/share?url='.urlencode( get_permalink( $post ) ),
      'popup'
    ),
    array(
      'Pinterest',
      'pinterest',
      'http://pinterest.com/pin/create/button/?url='.urlencode( get_permalink( $post ) ).'&amp;media='.get_bloginfo('url').@$image[0].'&amp;description='.urlencode( $post->post_excerpt),
      'popup'
    ),
    array(
      'Email',
      'email',
      'mailto:?subject='.$post->post_title.'&amp;body='.$post->post_excerpt.'%0A%0A'.urlencode( get_permalink( $post )),
      null
    )
  );

  foreach ($sharing as $share) {
    if(!empty($share[2])) {
      $href = ' href="'.$share[2].'"';
    } else {
      $href = null;
    }
?>

    <li class="rrssb-<?= $share[1] ?>">
      <!-- Replace subject with your message using URL Endocding: http://meyerweb.com/eric/tools/dencoder/ -->
      <a<?= $href ?> class="<?= $share[3] ?>">
        <span class="rrssb-icon"><?= @file_get_contents(get_stylesheet_directory().'/dist/images/rrssb/share-'.$share[1].'.svg') ?></span>
        <span class="rrssb-text"><?= $share[0] ?></span>
      </a>
    </li>

<?php } // end foreach ?>

  </ul><!--/.rrssb-buttons.clearfix-->

<?php } // share_buttons

function post_social_channels( $id = null ) {

  $meta = get_post_custom($id);
  $channels = [ 'Facebook', 'Twitter', 'LinkedIn', 'Instagram', 'Google-Plus', 'YouTube', 'Pinterest', 'TripAdvisor', 'Link'  ];
?>
  <ul class="social nolist">
<?php
  foreach ($channels as $channel) {
    $social = strtolower($channel);

    if(isset($meta['_social_channel_'.$social])) { ?>
      <li class="menu-<?= $social ?>"><a href="<?= $meta['_social_channel_'.$social][0] ?>" class="fa fa-<?= $social ?>" target="_blank"></a></li>
    <?php }
  } // foreach
?>
  </ul><!--/.social-->
<?php
} // post_social_channels

function copyright() {
?>

  <div class="copyright">
      <p>
      &copy;<?= date('Y'); ?>

      <?php

        if(!empty(get_option('company_info_company'))) {
          echo "<span>".stripslashes(get_option('company_info_company')).".</span> ";
        } else {
          echo "<span>".get_bloginfo('name').".</span> ";
        }

      ?><span>All rights reserved.</span>
      </p>

  </div><!--/.copyright-->

<?php
}

function privacy() {
    if (has_nav_menu('bottom_navigation')) :
      wp_nav_menu(['theme_location' => 'bottom_navigation', 'menu_class' => 'menu-legal nolist']);
    endif;
}

function location_info() {

  $fields = array(
    'address1',
    'address2',
    'city',
    'province',
    'country',
    'postal'
  );

  echo '<p class="location-info">';

  foreach($fields as $field) {

    if(!empty(get_option('company_info_'.$field))) {
      if( $field !== 'province' ) { echo '<span>'; } else {  }
      echo stripslashes(get_option('company_info_'.$field));
      if( $field == 'city' ) { echo ', '; } else { echo '</span>'; }
    }

  }

  echo '</p>';

}

function post_location_info($id = null) {

  $meta = get_post_custom($id);
  $fields = [ 'address1', 'address2', 'city', 'province', 'country', 'postal' ];

  echo '<p class="post-location-info">';

  foreach($fields as $field) {
    if(isset($meta['_location_info_'.$field])) {
      if( $field !== 'province' ) { echo '<span>'; }
      echo stripslashes($meta['_location_info_'.$field][0]);
      if( $field == 'city' ) { echo ', '; } else { echo '</span>'; }
    }
  }

  echo '</p>';

} // post_location_info

function contact_info() {

  $contactNumbers = array (
    array (
      'tollfree',
      'Toll Free'
    ),
    array (
      'phone',
      'Phone'
    ),
    array (
      'mobile',
      'Mobile'
    ),
    array (
      'fax',
      'Fax'
    ),
  );

  echo '<p class="contact-info">';

  foreach ($contactNumbers as $contactNumber) {

    if(!empty(get_option('company_info_'.$contactNumber[0]))) {
      $label = get_option( $contactNumber[1] );
      $number = get_option( 'company_info_'.$contactNumber[0] );

      if( !empty( get_option( 'company_info_'.$contactNumber[0]. '_ext' ))) {
        $ext = 'x '.get_option( 'company_info_'.$contactNumber[0] . '_ext' );
      } else {
        $ext = '';
      }

      echo "<span>".$contactNumber[1].": <a href='tel:".preg_replace("/[^0-9]/","",get_option( 'company_info_'.$contactNumber[0] ))."'>".get_option( 'company_info_'.$contactNumber[0] )."</a> ".$ext."</span>";
    }
  }

  if(!empty(get_option('company_info_email'))) {
    echo "<span><a href='mailto:".get_option("company_info_email")."'>".get_option("company_info_email")."</a></span>";
  }

  echo "</p>";

} // end get_contact_numbers

function post_contact_info($id = null) {

  $meta = get_post_custom($id);
  $numbers = [ ['Toll Free', 'tollfree'], ['Tel', 'tel'], ['Cell', 'cell'], ['Fax', 'fax']  ];

  if(isset($meta['_contact_info_tel_ext']))  {
    $ext = $meta['_contact_info_tel_ext'][0];
  } else {
    $ext = null;
  }

  echo "<p class='post-contact-info'>";

  foreach ($numbers as $num) {

    if(isset($meta['_contact_info_'.$num[1]])) {
      $url = "tel:".preg_replace( "/[^0-9]/","", $meta['_contact_info_'.$num[1]][0] );
      if( (!empty($ext)) && ($num[0] == 'Tel') ) { $extension = ' x '.$ext; } else { $extension = null; }
?>
      <span><?= $num[0]; ?>: <a href="<?= $url ?>"><?= $meta['_contact_info_'.$num[1]][0]; ?></a><?= $extension ?></span>
<?php
    }
  }

  if(isset($meta['_contact_info_email'])) {
    echo "<span><a href='mailto:".$meta['_contact_info_email'][0]."'>".$meta['_contact_info_email'][0]."</a></span>";
  }


} // post_contact_info

function hours( $set = null ) {

  // get next Monday
  $day_start = date( "d", strtotime( "next Monday" ) );

  $week_days = [];
  // create weekdays array.
  for ( $x = 0; $x < 7; $x++ )
  $week_days[] = date( "l", mktime( 0, 0, 0, date( "m" ), $day_start + $x, date( "y" ) ) );

  if(!empty($set)) {
    $hours = '_'.$set;
  } else {
    $hours = '';
  }


?>
  <ul class="hours nolist">
<?php
  if(!empty(get_option('company_info_weekday'.$hours))) { ?>

    <li>
      <strong>Weekdays:</strong>
      <span><?= get_option('company_info_weekday'.$hours) ?></span>
    </li>

  <?php }

  if(!empty(get_option('company_info_weekend'.$hours))) { ?>

    <li>
      <strong>Weekends:</strong>
      <span><?= get_option('company_info_weekend'.$hours) ?></span>
    </li>

  <?php }

  foreach ($week_days as $day) {
    $d = strtolower($day);
    $value = get_option('company_info_'.$d.$hours);

    if(!empty($value)) {
?>
      <li>
        <strong><?= $day ?>:</strong>
        <span><?= $value ?></span>
      </li>
<?php
    } // end if empty
  } // end weekday array

  if(!empty(get_option('company_info_alert'.$hours))) { ?>
    <li class="primary alert"><?= get_option('company_info_alert'.$hours) ?></li>
  <?php } ?>

  </ul><!--/.hours-->

<?php
} // end hours

function post_hours($id = null, $set = null) {

  if(!$id) {
    $id = get_the_ID();
  }

  if($set) {
    $set = '_'.$set;
  }

  $meta = get_post_custom($id);

  if (isset($meta['_hours_weekdays' . $set]) || isset($meta['_hours_monday' . $set])) {

  // get next Monday
  $day_start = date( "d", strtotime( "next Monday" ) );

  $week_days = [];
  // create weekdays array.
  for ( $x = 0; $x < 7; $x++ )
    $week_days[] = date( "l", mktime( 0, 0, 0, date( "m" ), $day_start + $x, date( "y" ) ) );

  ?>
  <ul class="hours nolist">
    <?php if (isset($meta['_hours_weekdays' . $set])) { ?>

      <li>
        <strong>Weekdays:</strong>
        <span><?= $meta['_hours_weekdays' . $set][0] ?></span>
      </li>

    <?php }

    if (isset($meta['_hours_weekends' . $set])) { ?>

      <li>
        <strong>Weekends:</strong>
        <span><?= $meta['_hours_weekends' . $set][0] ?></span>
      </li>

    <?php }

    foreach ($week_days as $day) {
      $d = strtolower($day);

      if(isset($meta['_hours_' . $d . $set])) {
        ?>
        <li>
          <strong><?= $day ?>:</strong>
          <span><?= $meta['_hours_' . $d . $set][0] ?></span>
        </li>
        <?php
      } // end if empty
    } // end weekday array

    if (isset($meta['_hours_alert' . $set])) { ?>
      <li class="primary alert"><?= $meta['_hours_alert' . $set][0] ?></li>
    <?php } ?>

  </ul><!--/.hours-->

<?php }
} // post_hours

add_action('footer_credit', __NAMESPACE__ .'\\credit');
function credit() {
?>
  <div class="jac-tech">
    <a href="http://jac.co" rel="external" title="JAC. We Create." id="jac">
      <?= @file_get_contents(get_template_directory()."/dist/images/by-jac.svg"); ?>
    </a>
  </div><!-- /.jac-tech -->
<?php
}


function hero_srcset( $size = 'hero', $id = null ) {

  if(empty($id)) {
    $id = get_post_thumbnail_id();
  }

  $hero = $id;

  // show default banner if one isn't defined.

  if (!empty($hero)) {

    $hero_xl = wp_get_attachment_image_src( $hero, $size.'-xl' );
    $hero_lg = wp_get_attachment_image_src( $hero, $size.'-lg' );
    $hero_md = wp_get_attachment_image_src( $hero, $size.'-md' );
    $hero_sm = wp_get_attachment_image_src( $hero, $size.'-sm' );
    $hero_xs = wp_get_attachment_image_src( $hero, $size.'-xs' );

    $hero_alt = get_post_meta( $hero, '_wp_attachment_image_alt', true );

    echo "<img src='".$hero_lg[0]."' srcset='".$hero_xs[0]." ".$hero_xs[1]."w, ".$hero_sm[0]." ".$hero_sm[1]."w, ".$hero_md[0]." ".$hero_md[1]."w, ".$hero_lg[0]." ".$hero_lg[1]."w, ".$hero_xl[0]." ".$hero_xl[1]."w' sizes='100vw' alt='".$hero_alt."' />";

  } else {

    $width = 1920;
    $height = 600;
    $path = '/app/themes/jac/dist/images/defaults/';

    $multipliers = [ 1, 0.75, 0.52083333, 0.33333333, 0.20833333 ];
    end($multipliers);
    $lastKey = key($multipliers);

    $x = 0;
?>
    <img src="<?= $path.'hero-default-'.round($width*$multipliers[1]).'x'.round($height*$multipliers[1]) . '.jpg' ?>" alt="<?php the_title(); ?>" srcset="<?php

      foreach($multipliers as $multiplier) {
        $w = round($width * $multiplier);
        $h = round($height * $multiplier);

        if( $lastKey == $x ) {
          $comma = '';
        } else {
          $comma = ', ';
        }

        echo $path.'hero-default-'.$w.'x'.$h.'.jpg '.$w.'w'.$comma;

        $x++;
      }

    ?>" />
<?php
  }
} // hero_srcset

function article_srcset( $id = null ) {

  if(empty($id)) {
    $id = get_post_thumbnail_id();
  }

  $feat = $id;

  if (!empty($feat)) {

    $feat_xl = wp_get_attachment_image_src( $feat, 'article-xl' );
    $feat_lg = wp_get_attachment_image_src( $feat, 'article-lg' );
    $feat_md = wp_get_attachment_image_src( $feat, 'article-md' );
    $feat_alt = get_post_meta( $feat, '_wp_attachment_image_alt', true );

    echo "<img src='".$feat_lg[0]."' srcset='".$feat_md[0]." ".$feat_md[1]."w, ".$feat_lg[0]." ".$feat_lg[1]."w, ".$feat_xl[0]." ".$feat_xl[1]."w' sizes='(min-width: 768px) 40vw, 100vw' alt='".$feat_alt."' />";

  } else {
    echo '<div class="feat-placeholder bg-gray-dark"></div>';
  }
}

function siblings( $heading = "In this Section", $h = "h3", $currentID = null ) {

  if(empty($currentID)) {
    $currentID = get_the_ID();
  }

  if (is_child()) {

    $parentID = wp_get_post_parent_id($currentID);

    $args = array(
      'post_parent'	=> $parentID,
      'post_status'	=> 'publish',
      'post_type'		=> 'page',
      'orderby'		=> 'menu_order',
      'order'			=> 'ASC',
    );

    $siblings = get_children($args);
?>
    <div class="tile-sidebar sidebar-siblings inner bg-gray-dark knockout">
      <?php echo '<'.$h.'>'.$heading.'</'.$h.'>'; ?>
        <ul class="nolist">
<?php
          foreach ($siblings as $post) {
            //echo '<pre>'.print_r ($child,true).'</pre>';
            $id = $post->ID;
            $slug = $post->post_name;
            $title = stripslashes($post->post_title);
            $subtitle = stripslashes($post->post_subtitle);
            $permalink = get_permalink($id);
            if ($currentID != $id) {
            	$active = null;
            } else {
            	$active = " active";
            }
?>
          <li class="menu-<?= $slug.$active ?>"><a href="<?= $permalink ?>" title="<?= $subtitle ?>"><?= $title ?></a></li>
<?php
          } // end foreach sibling
?>
        </ul>
      </div><!--/.siblings-->
<?php
  } //end if is_child()
} // end get_siblings()

function form_fields( $fields = [ ['Please Build Your Field Array'] ], $set = null ) {

  #array(
    #label,
    #name,
    #type,
    #class,
    #placeholder,
    #wrapperClass
  #),

  // $set is added to IDs and names as a prefix

  foreach ( $fields as $field ) {
    if (isset($field[0])) {
      $label = $field[0];
      $slug = preg_replace('/[^A-Za-z0-9\-]/', '', (str_replace(' ', '-', $field[0])));
    } else {
      $label = $slug = uniqid(); // default fall back
    }

    if (isset($field[1])) {
      $name = $field[1];
    } else {
      $name = '';
    }
    if (isset($field[2])) {
      $type = $field[2];
    } else {
      $type = 'text';
    }
    if (isset($field[3])) {
      $class = $field[3];
    } else {
      $class = '';
    }
    if (isset($field[4])) {
      $placeholder = $field[4];
    } else {
      $placeholder = null;
    }
    if (isset($field[5])) {
      $value = $field[5];
    } else {
      $value = null;
    }
    if (isset($field[6])) {
      $wrapperClass = $field[6];
    } else {
      $wrapperClass = null;
    }
?>

<?php if ($type == 'textarea') { ?>

  <div class="input-wrap <?= $type ?>-wrap <?= $wrapperClass ?>">
    <textarea
      id="<?= $name ?>-<?= $slug ?>"
      name="<?= $name ?>"
      class="<?= $class ?>"
      placeholder="<?= $placeholder ?>"
    ><?= $value ?></textarea>
    <label for="<?= $name ?>-<?= $slug ?>"><?= $label ?></label>
  </div><!--/.input-wrap <?= $type ?>-wrap-->

<?php } else { ?>

  <div class="input-wrap <?= $type ?>-wrap">
    <input
      id="<?= $name ?>-<?= $slug ?>"
      type="<?= $type ?>"
      name="<?= $name ?>"
      class="<?= $class ?>"
      placeholder="<?= $placeholder ?>"
      value="<?= $value ?>"
    />
  <label for="<?= $name ?>-<?= $slug ?>"><?= $label ?></label>
  </div><!--/.input-wrap <?= $type ?>-wrap-->

<?php } ?>

<?php
    } // end foreach field
} // get_form_fields()


function media_gallery() {
  if(current_theme_supports('media-gallery')) {

  /*
    NOTE:
    loop through videos first.
    Then loop through photos from gallery

    Adjust the ajax pagination based on screen size:
      - from smallest to largest,
      - display one item, one row until 568px
      - display two items, one row until 992px
      - display six items, two rows until 1200px
      - display eight items, two rows after 1200px
  */

  global $post;
  $videos = \JAC\Video\get_videos($post->ID);
  $images = \JAC\Gallery\get_images($post->ID);

  // echo '<pre>'.print_r ($videos,true).'</pre>';
  // echo '<pre>'.print_r ($images,true).'</pre>';

  if (!empty($videos) || !empty($images)) { ?>
    <section class="section-gallery js-scope-wrap" data-action="the_media_gallery_grid" data-paged="1" data-postid="<?= $post->ID ?>" data-type="all">

      <!-- <div class="section titlebar blurb">
        <h2>Media Gallery</h2>
      </div> -->

      <div class="pagination-wrap bg-gray-darker knockout">
        <nav class="pagination container js-filter-area">
          <div class="title">

            <?php if(!empty($images) && !empty($videos)) { ?>

            <ul class="pagination-filter nolist">
              <li><a class="js-bar-item selected" data-name="type" data-value="all">All</a>
              <li><a class="js-bar-item" data-name="type" data-value="photo">Images</a>
              <li><a class="js-bar-item" data-name="type" data-value="video">Videos</a>
            </ul>

            <?php } else { ?>
              <p class="subtitle">
                Click on the thumbnail to
                <?php
                  if(!empty($images)) {
                    echo "enlarge the image.";
                  } elseif(!empty($videos)) {
                    echo "play the video.";
                  }
                ?>
              </p>
            <?php } ?>

          </div><!--/.title-->
          <div class="arrows pagi">
            <a class="disabled js-bar-item js-filter-prev"><i class="fa fa-angle-left"></i></a>
            <a class="disabled js-bar-item js-filter-next"><i class="fa fa-angle-right"></i></a>
          </div><!--/.arrows-->
        </nav><!--/.pagination-->
      </div><!--/.pagination-wrap-->
      <div class="section container">
        <div class="section-grid js-the-response-grid">
          <?php (new \Ajax())->the_media_gallery_grid(); ?>
        </div><!--/.gallery-wrap-->
      </div><!--/.container-->
    </section><!--/.section-gallery-->
<?php }
  } // end if theme supports gallery
} // end media_gallery()

function media_item( $item = '' ) {
  // $type prints to a class and css targets it.

  if (is_numeric($item)) {
    $type = 'image';
    $full = wp_get_attachment_image_src($item, 'full')[0];
    $thumb = wp_get_attachment_image_src($item, 'gallery-thumbnail')[0];
    $title = get_post($item)->post_title;
  } else {
    $type = 'video';
    $full = $item;
    $thumb = '//img.youtube.com/vi/' . get_youtube_id($item) . '/0.jpg'; // 480x360 pixels
    $title = get_youtube_title($item);
  }
?>

  <div class="gallery-item gallery-<?= $type ?>">
    <a href="<?= $full ?>" class="js-popup" data-gallery="gallery" title="Expand <?= $title ?> (<?= $type ?>)">
      <img src="<?= $thumb; ?>" alt="<?= $title ?>" />
    </a>
  </div>

<?php

} // end media_item

function get_youtube_id ($url) {
  //http://stackoverflow.com/a/17030234/1265856
  preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
  return $matches[1];
}

function get_youtube_title ($url) {
  $vid = get_youtube_id($url);
  $response = wp_remote_get('http://www.youtube.com/get_video_info?video_id=' . $vid);
  if (is_array($response) && is_string($response['body'])) {
    parse_str($response['body'], $arr);
    if (isset($arr['title'])) return $arr['title'];
  }
  return 'Youtube Video';
}

function archives($args = null) {
  global $wpdb;

  $defaults = array(
    'post_type' => 'post',
    'order' => 'DESC'
  );

  $a = wp_parse_args($args, $defaults);

  $archives = $wpdb->get_results("
			SELECT $wpdb->posts.ID, $wpdb->posts.post_name, $wpdb->posts.post_title, $wpdb->posts.post_date, YEAR($wpdb->posts.post_date) AS 'year', MONTH($wpdb->posts.post_date) AS 'month'
			FROM $wpdb->posts
			WHERE post_date != '0000-00-00 00:00:00' AND post_status = 'publish'
			AND post_type = '" . $a['post_type'] . "'
			GROUP BY $wpdb->posts.ID
			ORDER BY $wpdb->posts.post_date " . $a['order']
  );

  $archives_togo = [];
  foreach ($archives as $arc) :
    $archives_togo[$arc->year]['month'][$arc->month]['posts'][] = $arc;
    if (!isset($archives_togo[$arc->year]['count'])) $archives_togo[$arc->year]['count'] = 0;
    $archives_togo[$arc->year]['count'] += 1;
    if (!(isset($archives_togo[$arc->year][$arc->month]['count']))) $archives_togo[$arc->year][$arc->month]['count'] = 0;
    $archives_togo[$arc->year][$arc->month]['count'] += 1;
  endforeach;
  return $archives_togo;
} // archives

function popup_feedback() { ?>
  <div class="popup-wrap popup-wrap-feedback">
    <div class="close-bg js-popup-feedback"></div>
    <div class="popup-modal bg-white">
      <div class="inner">
        <form action="<?php the_permalink(); ?>" method="post" class="js-popup-feedback-form">
          <p class="js-message">Fill out the form below to leave feedback about the website and your browsing experience.</p>
          <?php
          $fields = [
            [ 'Email', 'email', 'email' ],
            [ 'Message', 'message', 'textarea' ],
          ];
          \JAC\Get\form_fields($fields);
          ?>
          <input type="hidden" name="url" value="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>">
          <button type="submit" class="js-fields">Submit</button>
        </form>
      </div><!--/.inner-->
      <div class="popup-cancel knockout bg-primary js-popup-feedback">
        <i class="fa fa-times"></i>
      </div><!--/.popup-cancel-->
    </div><!--/.popup-modal-->
  </div><!--/.popup-wrap-feedback-->
<?php } // popup_feedback
add_action('wp_ajax_popup_feedback_submit', __NAMESPACE__ . '\\popup_feedback_submit');
add_action('wp_ajax_nopriv_popup_feedback_submit', __NAMESPACE__ . '\\popup_feedback_submit');
function popup_feedback_submit() {

  parse_str($_REQUEST['data'], $data);

  if (empty($data['email'])) {
    wp_send_json_error(['message' => 'Error: Email is empty.']);
  } else if (!is_email($data['email'])) {
    wp_send_json_error(['message' => 'Error: This is not an email.']);
  } else if (empty($data['message'])) {
    wp_send_json_error(['message' => 'Error: Message is empty.']);
  }

  $headers = [];
  $headers[] = 'Content-Type: text/html; charset=UTF-8';

//  $headers[] = 'From: Client Website Feedback <feedback@forms.jac.co>';
//  $headers[] = 'Bcc: JAC Developers <developers@jac.co>';
  $headers[] = 'Reply-To: ' . sanitize_text_field($data['email']) . ' <' . $data['email'] . '>';

  // http://stackoverflow.com/a/14985633/1265856
  if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
  }

  $data['message'] = "Website: " . get_home_url() . "\n\nCurrent URL: " . $data['url'] . "\n\nSender IP: " . $_SERVER['REMOTE_ADDR'] . "\n\nMessage:\n" . wp_unslash(esc_html($data['message']));

  $result = wp_mail(get_option('admin_email'),  '[Website Feedback] ' . htmlspecialchars_decode(get_bloginfo('name')), wpautop($data['message']), $headers);

  if ($result) {
    wp_send_json_success();
  } else {
    wp_send_json_error(['message'=> 'Server error, please send us email: developers@jac.co']);
  }

}

function popup_search() { ?>

<div class="popup-wrap popup-wrap-search">
  <div class="close-bg js-popup-search"></div>
  <div class="popup-modal">

    <h2>Search
    <?php

      if(!empty(get_option('company_info_company'))) {
        echo "<span>".stripslashes(get_option('company_info_company'))."</span> ";
      } else {
        echo "<span>".get_bloginfo('name')."</span> ";
      }

    ?>
    </h2>
    <?php get_search_form(); ?>
    <div class="popup-cancel js-popup-search">
      <p>Cancel</p>
    </div>

  </div><!--/.popup-modal-->
</div><!--/.popup-wrap-search-->

<?php } // popup_search

if(get_option('fb_messenger_enabled')) {
  add_action( 'wp_footer', __NAMESPACE__ . '\\facebook_messenger' );
}
function facebook_messenger() {
  if(!empty( get_page_by_path( 'facebook', 'object', 'social_channel' ) )) {
    $facebook = get_page_by_path( 'facebook', 'object', 'social_channel' );
    $facebook_meta = get_post_custom($facebook->ID);
    $facebook_url = $facebook_meta['_social_url'];

    if(!empty($facebook_url)) {
?>
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <div class="fb-messenger-button js-fb-messenger">
        <span class="fa-stack">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-comments fa-stack-1x"></i>
        </span>

        <div class="fb-bar">
          <p><i class="fa fa-comments"></i> Message <?= stripslashes(get_option('company_info_company')) ?></p>
        </div><!--/.fb-bar-->

      </div><!--/.fb-messenger-button-->
      <div class="fb-messenger-wrap">
        <div class="close-bg js-fb-messenger"></div>
        <i class="fb-messenger-close js-fb-messenger fa fa-times"></i>
        <div id="fb-root"></div>
        <div
          class="fb-page"
          data-width="300"
          data-height="400"
          data-href="<?= $facebook_url[0] ?>"
          data-tabs="messages" data-small-header="true"
          data-adapt-container-width="true" data-hide-cover="true"
          data-show-facepile="false"
        >
        <blockquote
          cite="<?= $facebook_url[0] ?>"
          class="fb-xfbml-parse-ignore"
        >
          <a href="<?= $facebook_url[0] ?>">
            <?= stripslashes(get_option('company_info_company')) ?>
          </a>
        </blockquote>
      </div>
    </div><!--/.fb-messenger-wrap-->
<?php
    }
  }
} // facebook_messenger()
