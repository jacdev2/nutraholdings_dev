<?php

namespace JAC\Calendar;

new Ajax();
class Ajax {

    public function __construct() {

        $functions = get_class_methods($this);

        foreach ($functions as $function) {
            if ($function[0] == '_') continue;
            add_action( 'wp_ajax_jac_calendar_' . $function, [$this, $function] );
            add_action( 'wp_ajax_nopriv_jac_calendar_' . $function, [$this, $function] );
        }

    }

    private function _exit($msg = '') {
        if (defined('DOING_AJAX') && DOING_AJAX) wp_die($msg);
    }

    public function get_body() {

        if (empty($_REQUEST['go']) || !is_numeric($_REQUEST['year']) || !is_numeric($_REQUEST['month'])) $this->_exit('Error');

        $go = $_REQUEST['go'];

        $year = empty($_REQUEST['year']) ? date('Y', time()) : $_REQUEST['year'];
        $month = empty($_REQUEST['month']) ? date('m', time()) : $_REQUEST['month'];

        if ($go == 'prev') {
            $timestamp = strtotime('-1 month', strtotime($year . '-' . $month . '-01'));
        } else if ($go == 'next') {
            $timestamp = strtotime('+1 month', strtotime($year . '-' . $month . '-01'));
        } else {
            $timestamp = strtotime($year . '-' . $month . '-01');
        }

        $cal = new Calendar($timestamp);

        $cal->get_weekdays();
        $cal->get_days();

        $this->_exit();

    }

    public function get_event() {

        if (empty($_REQUEST['paged'])) wp_send_json_error();

        if (!isset($_REQUEST['year'])) $_REQUEST['year'] = null;
        if (!isset($_REQUEST['month'])) $_REQUEST['month'] = null;
        if (!isset($_REQUEST['day'])) $_REQUEST['day'] = null;

        $event = new Event(null, $_REQUEST['year'], $_REQUEST['month'], $_REQUEST['day']);
        $event->setup_event_query($_REQUEST['paged']);

        if ($event->get_total_posts()) {

            $prev = false;
            $next = false;

            if ($_REQUEST['paged'] > 1) $prev = true;
            if ($_REQUEST['paged'] < $event->get_total_pages()) $next = true;

            wp_send_json_success([
                'prev' => $prev,
                'next' => $next,
                'html' => $event->get_event()
            ]);

        } else {
            wp_send_json_success([
                'prev' => false,
                'next' => false,
                'html' => $event->get_event()
            ]);
        }

    }

}


class Calendar {

    public $year, $month;

    public function __construct($timestamp = null, $year = null, $month = null) {

        $_getYmd = function ($timestamp) {
            $this->year = date('Y', $timestamp);
            $this->month = date('m', $timestamp);
        };

        if (!is_null($timestamp)) {
            $_getYmd($timestamp);
        } else if (!is_null($year) && !is_null($month)) {
            $this->year = $year;
            $this->month = $month;
        } else {
            $_getYmd(time());
        }

    }

    public function get_year() {
        return $this->year;
    }

    public function get_month() {
        return $this->month;
    }

    public function get_title() {
        return date('F Y', strtotime($this->year . '-' . $this->month . '-01'));
    }

    public function get_weekdays() {
        foreach (['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'] as $weekday) { ?>
            <div class="calendar-weekday calendar-day"><?= $weekday ?></div>
        <?php }
    }

    public function get_days() {

        $first_day = mktime(0, 0, 0, $this->month, 1, $this->year); // get time stamp of 1st day of this month
        $blank = date('w', $first_day);

        $days_in_month = cal_days_in_month(0, $this->month, $this->year);

        // counts the days in the week, up to 7
        $day_count = 1;
        // sets the first day of the month to 1
        $day_num = 1;

        while ( $blank > 0 ) { ?>
            <div class="calendar-day calendar-blank">
            </div>
            <?php
            $blank -= 1;
            $day_count += 1;
        }

        while ( $day_num <= $days_in_month ) {
            $day = sprintf('%02d', $day_num);
            $class = [];
            if ($this->year . '-' . $this->month . '-' . $day == date('Y-m-d', time())) $class[] = 'current';
            $event = new Event(null, $this->year, $this->month, $day);
            if ($event->get_total_posts()) $class[] = 'booked';
            ?>
            <div class="calendar-day js-day<?php if(!empty($class)) echo ' '; echo implode(' ', $class); ?>">
                <div class="click-area js-select<?php if (in_array('booked', $class)) echo ' js-booked'; ?>" data-day="<?= $day ?>">
                    <span><?= $day ?></span>
                </div>
            </div>
            <?php
            $day_num += 1;
            $day_count += 1;

            if ($day_count > 7) {
                $day_count = 1;
            }

        }

        while ( $day_count > 1 && $day_count <= 7 ) { ?>
            <div class="calendar-day calendar-blank">
            </div>
            <?php $day_count += 1;
        }

    }

}


if (is_admin()) new MetaBox();
class MetaBox {

    public function __construct() {

        add_theme_support('event-time');

        add_action('add_meta_boxes', [$this, 'add_meta_box']);
        add_action('save_post', [$this, 'save']);

    }

    public function add_meta_box($post_type) {

        if ( post_type_supports( $post_type, 'event-time' ) && current_theme_supports( 'event-time' ) ) {

            add_meta_box(
                'event-time',
                'Event Time',
                [$this, 'event_time'],
                $post_type,
                'normal',
                'high'
            );

        }

    }

    public function event_time($post) {
        wp_nonce_field( 'event_time_nonce', 'event_time_nonce_field' );

        $start = (int)get_post_meta($post->ID, '_jac_event_start', true);
        $end = (int)get_post_meta($post->ID, '_jac_event_end', true);

        if ($end < $start) $start = $end;

        echo '<input type="hidden" class="js-event-time-ts-start" name="event[start]" value="' . $start . '">';
        echo '<input type="hidden" class="js-event-time-ts-end" name="event[end]" value="' . $end . '">';

        ?>

        <div class="js-event-time">
            <label>Start Time: </label>
            <input type="text" class="js-event-time-start" placeholder="Event Start" value="<?= wpdate('F j, Y, g:i a', $start) ?>">
            <label>End Time: </label>
            <input type="text" class="js-event-time-end" placeholder="Event End" value="<?= wpdate('F j, Y, g:i a', $end) ?>">
        </div>

    <?php }

    public function save($post_id) {

        // Check if our nonce is set.
        if (!isset($_POST['event_time_nonce_field']))
            return;

        $nonce = $_POST['event_time_nonce_field'];

        // Verify that the nonce is valid.
        if (!wp_verify_nonce($nonce, 'event_time_nonce'))
            return;

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;

        /* OK, its safe for us to save the data now. */

        if (isset($_REQUEST['event'])) {

            foreach ($_REQUEST['event'] as $field => $value) {

                if (!empty($value)) {
                    add_post_meta($post_id, '_jac_event_' . $field, (int)$value, true) or
                    update_post_meta($post_id, '_jac_event_' . $field, (int)$value);
                } else {
                    delete_post_meta($post_id, '_jac_event_' . $field);
                }
            }

        }

    }

}


class Event {

    public $year, $month, $day;
    public $timestamp;

    public $posts_per_page = 6;
    public $meta_key = '_jac_event_start';

    private $the_event_query;

    public function __construct($timestamp = null, $year = null, $month = null, $day = null) {

        if (!is_null($timestamp)) {

            $this->timestamp = $timestamp;

            $this->year = date('Y', $timestamp);
            $this->month = date('m', $timestamp);
            $this->day = date('d', $timestamp);

        } else {

            $this->year = $year;
            $this->month = $month;
            $this->day = $day;

        }

    }

    public function set_posts_per_page($number) {
        if (is_numeric($number)) $this->posts_per_page = $number;
    }

    public function setup_event_query($paged = 1) {

        $args = [
            'posts_per_page' => $this->posts_per_page,
            'paged' => $paged,
            'post_type' => 'event',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_key' => $this->meta_key,
        ];

        if (!is_null($this->year) && !is_null($this->year) && !is_null($this->year)) {

            $start = strtotime($this->year . '-' . $this->month . '-' . $this->day . ' 00:00:00');
            $end = strtotime($this->year . '-' . $this->month . '-' . $this->day . ' 23:59:59');

            $args['meta_query'] = [
                [
                    'key' => $this->meta_key,
                    'value' => [$start, $end],
                    'type' => 'numeric',
                    'compare' => 'BETWEEN',
                ],
            ];

        } else {
            $args['meta_query'] = [
                [
                    'key' => $this->meta_key,
                    'value' => time(),
                    'compare' => '>=',
                ],
            ];
        }

        $this->the_event_query =  new \WP_Query($args);

    }

    public function get_event() {

        if (empty($this->the_event_query)) $this->setup_event_query();

        ob_start();

        if ($this->the_event_query->post_count) {

            foreach ($this->the_event_query->posts as $item) {
                print_r($this->the_event_query->posts);
                $start = (int)get_post_meta($item->ID, '_jac_event_start', true);
                $end = (int)get_post_meta($item->ID, '_jac_event_end', true);
                $content = get_the_content($item);

                if($content){
                    ?>
                    <a class="calendar-event" href="<?= get_permalink($item) ?>" title="view event">
                    <?php
                }else{
                    ?>
                    <div class="calendar-event">
                    <?php
                }
                ?>

                <div class="inner">
                    <h3><?= get_the_title($item) ?></h3>
                    <p><?php
                        if ($end - $start > 86400) {
                            echo wpdate('M j', $start);
                        } else {
                            echo wpdate('g:i A', $start);
                        } ?></p>
                </div><!--/.inner-->
                <p class="timestamp"><span><?= wpdate('M', $start) ?></span><strong><?= wpdate('d', $start) ?></strong></p>
                <?php if($content){ ?><div class="more"><i class="fa fa-angle-right"></i></div><?php }
                if($content){
                    ?>
                    </a>
                    <?php
                }else{
                    ?>
                    </div>
                    <?php
                }
                ?>

            <?php }

        } else {

            if (!is_null($this->year) && !is_null($this->year) && !is_null($this->year)) {
                $ts = strtotime($this->year . '-' . $this->month . '-' . $this->day);
                ?>
                <div class="calendar-event">
                    <div class="inner">
                        <h3><?= pll_e('There are no events on this day') ?></h3>
                    </div><!--/.inner-->
                    <p class="timestamp"><span><?= date('M', $ts) ?></span><strong><?= date('d', $ts) ?></strong></p>
                    <div class="more"></div>
                </div><!--/.calendar-event-->

            <?php } else { ?>

                <div class="calendar-event no-events">
                    <div class="inner">
                        <h3><?= pll_e('There are no upcoming events scheduled at this time') ?></h3>
                    </div><!--/.inner-->
                    <div class="more"></div>
                </div><!--/.calendar-event-->

            <?php }
        } ?>
        <?php if(is_page_template(['overview-categories.php'])){ ?>
            <a href="<?= get_post_type_archive_link( 'event' ) ?>" class="button primary past-events"><?= pll_e('View All Events')?></a>
        <?php }
        else { ?>

            <a href="<?= get_post_type_archive_link( 'event' ) ?>" class="button primary past-events"><?= pll_e('View Past Events')?></a>

        <?php } ?>
        <?php return ob_get_clean();
    }

    public function get_total_posts () {
        if (empty($this->the_event_query)) $this->setup_event_query();
        return $this->the_event_query->found_posts;
    }

    public function get_total_pages () {
        if (empty($this->the_event_query)) $this->setup_event_query();
        return $this->the_event_query->max_num_pages;
    }

}
