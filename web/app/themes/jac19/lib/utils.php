<?php

/**
 *
 * get post feature image
 *
 * @since Apr 2016
 *
 * @param int|WP_Post  $post Optional. Post ID or WP_Post object.  Default is global `$post`.
 * @param string|array $size Optional.  Default is 'full'.
 * @return string The post thumbnail image url.
 */
function get_the_image ($post = 0, $size = 'full') {
  $post = get_post( $post );
  $attachment_id = get_post_thumbnail_id( $post );
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  if ($image) return $image[0];
  else return null;
}

/**
 * Display or retrieve the current post title with optional content.
 *
 * @see post-template.php:42
 *
 * @since Apr 2016
 *
 * @param string $before Optional. Content to prepend to the title.
 * @param string $after  Optional. Content to append to the title.
 * @return string|void String if $echo parameter is false.
 */
function the_subtitle( $before = '', $after = '' ) {
  $subtitle = get_the_subtitle();
  $subtitle = $before . $subtitle . $after;
  echo $subtitle;
}

/**
 * Retrieve post subtitle.
 *
 * @see post-template.php:110
 *
 * @since Apr 2016
 *
 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global $post.
 * @return string
 */
function get_the_subtitle( $post = 0 ) {
  $post = get_post( $post );

  $subtitle = isset( $post->post_subtitle ) ? $post->post_subtitle : '';
  $id = isset( $post->ID ) ? $post->ID : 0;

  return apply_filters( 'the_subtitle', $subtitle, $id );
}

/**
 * get page id by path slug. search page first then all post types
 *
 * @since Apr 2016
 *
 * @param string $path page path/slug
 * @return int|null page id on success
 */
function get_id_by_path ($path) {
  $p = get_page_by_path($path);
  if (is_null($p)) $p = get_page_by_path($path, OBJECT, get_post_types());
  if (is_null($p)) return null;
  else return $p->ID;
}

/**
 * check current post's post type
 *
 * @since May 2016
 *
 * @param string|array $post_type post type string or array
 * @return bool true if it is given post type
 */
function is_post_type( $post_type ) {

  if ( is_array( $post_type ) && in_array( get_post_type(), $post_type ) ) {
    return true;
  } elseif ( get_post_type() == $post_type ) {
    return true;
  } else {
    return false;
  }

}

/**
 * check if current post has children post
 *
 * @since May 2016
 *
 * @return bool true if it is parent post
 */
function is_parent() {

  $id = get_the_ID();

  $args = array(
    'posts_per_page'   => - 1,
    'post_parent'      => $id,
    'post_type'        => 'any',
    'suppress_filters' => false,
  );

  $children = get_posts( $args );

  if ( $children ) {
    return true;
  } else {
    return false;
  }

}

/**
 *
 * check if current post has parent post
 *
 * @since May 2016
 *
 * @return bool true if post has parent post
 */
function is_child() {

  global $post;

  if  ($post->post_parent != 0) {
    return true;
  } else {
    return false;
  }

}

/**
 * Add up to five image sizes with the same aspect ratio.
 *
 * @see post-template.php:42
 *
 * @since Apr 2016
 *
 * @param string $name image size name
 * @param int $w width of image
 * @param int $h height of image
 * @param bool $crop crop of image
 * @param int $total how many size need? from xl to xs
 */
function add_image_srcset( $name = 'size', $w = null, $h = null, $crop = true, $total = 5 ) {

  if(empty($h)) { $h = $w; }
  if( $total > 5 ) { $total = 5; }

  $x = 1;

  $multipliers = [
    [ 1, 'xl' ],[ 0.75, 'lg' ],[ 0.52083333, 'md' ],[ 0.33333333, 'sm' ],[ 0.20833333, 'xs' ]
  ];

  foreach ( $multipliers as $multiplier ) {

    if( $x <= $total ) {

      add_image_size (
        $name.'-'.$multiplier[1],
        round($w * $multiplier[0]),
        round($h * $multiplier[0]),
        $crop
      );
    }
    $x++;
  }
}

/**
 * Add wordpress time zone offset to PHP date()
 *
 * The 'mysql' type will return the time in the format for MySQL DATETIME field.
 * Other strings will be interpreted as PHP date formats (e.g. 'Y-m-d').
 *
 * @since Dec 2016
 *
 * @param string   $type      Type of time to retrieve. Accepts or PHP date format string (e.g. 'Y-m-d').
 * @param int      $timestamp Optional. Unix time stamp. Default current time.
 * @return string|bool        A formatted date string.
 */
function wpdate($type, $timestamp = 0) {

  if (empty($timestamp)) $timestamp = time();
  else if (!is_numeric($timestamp)) $timestamp = intval($timestamp);
  if ($type == 'mysql') $type = 'Y-m-d H:i:s';

  $offset = get_option('gmt_offset');
  $tz = get_option('timezone_string');

  if (!empty($offset)) {
    return date($type, $timestamp + ($offset * HOUR_IN_SECONDS));
  } elseif (!empty($tz)) {
    $datetime = new DateTime();
    $datetime->setTimestamp($timestamp);
    $datetime->setTimezone( new DateTimeZone( $tz ) );
    $string_localtime = $datetime->format( $type );
    return $string_localtime;
  } else {
    return date($type, $timestamp);
  }

}

/**
 * check if current post type has an archive
 *
 * check the post type object of the currently displayed post.
 *
 * @since Dec 2016
 *
 * @param int|WP_Post $post   Optional. Post ID or post object. Defaults to global $post.
 * @return bool true if post type has archive
 */
function has_archive($post = 0) {
  $post = get_post( $post );
  $postType = get_post_type_object( $post->post_type );

  if($postType->has_archive) {
    return true;
  } else {
    return false;
  }

} // has_archive

/**
 * check if current post type has a rewrite rule
 *
 * check the post type object of the currently displayed post.
 *
 * @since Dec 2016
 *
 * @param int|WP_Post $post   Optional. Post ID or post object. Defaults to global $post.
 * @return bool true if post type has archive
 */
function has_rewrite($post = 0) {
  $post = get_post( $post );
  $postType = get_post_type_object( $post->post_type );
  $rewrite = $postType->rewrite;

  if($rewrite) {
    return true;
  } else {
    return false;
  }

} // has_redirect

/**
 * redirect single posts if they're not meant to be displayed
 *
 * check the post type object of the currently displayed post
 * has an archive or rewrite and redirect accordingly
 *
 * @since Dec 2016
 */
//add_action('template_redirect', 'jac_wp_redirect_single', 100);
function jac_wp_redirect_single($post) {
  global $post;

  $redirect_single = get_post_meta(get_the_ID(), '_redirect_single', true);
  $template = get_post_meta($post->post_parent, '_wp_page_template', true);
  $parentURL = get_the_permalink($post->post_parent);
  // echo $redirect_single . $template . $parentURL;

  if(!empty($post) && is_single()) {

    $postType = get_post_type_object( $post->post_type );
    $rewrite = $postType->rewrite;
    $path = '/' . strtolower($postType->label);

    if($redirect_single && $template == 'overview.php' && $post->post_parent) {
      wp_redirect( get_the_permalink($parentURL), 301);
      exit;
    } elseif( !has_rewrite($post) && !has_archive($post) ) {
      wp_redirect( home_url(), 301 );
      exit;
    } elseif(has_rewrite($post) && post_type_supports($post->post_type, 'linkage')) {
      wp_redirect( home_url('/'.$rewrite['slug']), 301 );
      exit;
    }

  } // endif $post && is_single()

} // jac_wp_redirect_single

/**
 * redirect archive to single post if there is only one post.
 *
 * $post object refers to the first post while on an archive page
 * redirect is set as 302 temporary
 *
 * @since Dec 2016
 */
//add_action('template_redirect','jac_wp_redirect_archive_one_post', 101);
function jac_wp_redirect_archive_one_post() {

  if( (is_post_type_archive() || is_home() || is_tax() || is_tag() || is_category()) && !empty(get_post_type()) ) {
    $total = wp_count_posts(get_post_type())->publish;
    if( $total == 1 && !post_type_supports(get_post_type(), 'linkage') ) {
      wp_redirect( get_the_permalink(), 302 );
      exit;
    }
  }

} // jac_wp_redirect_archive_one_post
