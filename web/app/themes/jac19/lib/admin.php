<?php

namespace JAC\Admin;

/**
 * menu order quick change
 */
//ajax
add_action('wp_ajax_menu_order_quick_change', function () {
  if ((int)$_GET['order'] && $_GET['id']) {
    if (wp_update_post
      ([
        'ID' => $_GET['id'],
        'menu_order' => $_GET['order']
      ])
    )
      wp_send_json_success();
  }
  wp_send_json_error();
});
// column
add_action('manage_page_posts_custom_column', __NAMESPACE__ . '\\manage_custom_column', 10, 2);
function manage_custom_column( $column_name, $post_id ) {
  $post = get_post($post_id);
  switch ($column_name) {
    case 'image':
      $post_thumbnail_id = get_post_thumbnail_id($post_id);
      if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id);
        $post_full_img = wp_get_attachment_image_src($post_thumbnail_id, 'full');
        echo '<a href="' . $post_full_img[0] . '" class="js-popup"><img src="' . $post_thumbnail_img[0] . '" /></a>';
      }
      break;
    case 'order' :
      echo '<input type="number" class="js-menu-order-quick-change" data-id="' . $post_id . '" value="' . $post->menu_order . '">';
      break;
  }
}
// add image and order columns to page
add_filter('manage_pages_columns', function ($posts_columns) {
  return array_merge(array_splice( $posts_columns, 0, 1 ), ['image' => ''], $posts_columns, ['order' => 'Order']);
});

/**
 * hide some columns by default
 */
add_filter( 'default_hidden_columns', function ( $hidden, $screen ) {
  switch ($screen->post_type) {
    case 'post':
      $hidden[] = 'tags';
      $hidden[] = 'categories';
      break;
    case 'page':
      $hidden[] = 'author';
      $hidden[] = 'date';
      break;
    default:
      $hidden[] = 'date';
  }
  return $hidden;
}, 10, 2 );

/**
 * editor can manage user
 * @link http://isabelcastillo.com/editor-role-manage-users-wordpress
 */
add_filter('editable_roles', function ($roles) {
  if (isset($roles['administrator']) && !current_user_can('administrator')) {
    unset($roles['administrator']);
  }
  return $roles;
});
add_filter('map_meta_cap', function ($caps, $cap, $user_id, $args) {
  switch( $cap ){
    case 'edit_user':
    case 'remove_user':
    case 'promote_user':
      if (isset($args[0]) && $args[0] == $user_id)
        break;
      elseif (!isset($args[0]))
        $caps[] = 'do_not_allow';
      $other = new \WP_User(absint($args[0]));
      if ($other->has_cap('administrator')) {
        if (!current_user_can('administrator')) {
          $caps[] = 'do_not_allow';
        }
      }
      break;
    case 'delete_user':
    case 'delete_users':
      if (!isset($args[0]))
        break;
      $other = new \WP_User(absint($args[0]));
      if ($other->has_cap('administrator')) {
        if (!current_user_can('administrator')) {
          $caps[] = 'do_not_allow';
        }
      }
      break;
    default:
      break;
  }
  return $caps;
}, 10, 4);
add_action('init', function () {
  if (empty(get_option('editor_cap_setup'))) {

    $edit_editor = get_role('editor');

    $edit_editor->add_cap('edit_users');
    $edit_editor->add_cap('delete_users');
    $edit_editor->add_cap('create_users');
    $edit_editor->add_cap('list_users');
    $edit_editor->add_cap('remove_users');
    $edit_editor->add_cap('add_users');
    $edit_editor->add_cap('promote_users');

    $edit_editor->add_cap('edit_theme_options');

    add_option('editor_cap_setup', 1);
  }
});
