<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/admin.php',     // JAC admin
  'lib/setup-jac.php', // JAC setup
  'lib/ajax.php',      // JAC ajax
  'lib/get.php',       // JAC Front End Functions
  'lib/admin-menu.php',// JAC admin menu
  'lib/admin-ajax.php',// JAC admin ajax
  'lib/post-types.php',// JAC post types
  'lib/meta-boxes.php',// JAC meta boxes
  'lib/utils.php',// JAC meta boxes
  'lib/taxonomies.php',// JAC taxonomies
  'lib/calendar.php',// JAC Calendar & Events
  'lib/gallery.php',   // JAC gallery
  'lib/video.php',     // JAC video
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


function get_child_links($parentId){

    $query = new WP_Query([
        'post_type' => 'page',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => -1,
        'post_parent' => $parentId
    ]);

    if ($query->have_posts()) { ?>
        <ul class="nolist">
          <?php
            while ($query->have_posts()) {
                global $post;
                $query->the_post();

                    $title = get_the_title();
                ?>
                <li><a href="<?= the_permalink(); ?>"><?= $title; ?></a></li>
                <?php
            }
          ?>
        </ul>

      <?php 
    }
    wp_reset_postdata();
}

function get_child_posts($parentId){

    $query = new WP_Query([
        'post_type' => $parentId,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => -1,
    ]);

    if ($query->have_posts()) { ?>
        <ul class="nolist nolistevent">
          <?php
            while ($query->have_posts()) {
                global $post;
                $query->the_post();
                    $doc = get_post_meta(get_the_ID(), '_doc', true);
                    $linkage_text = get_post_meta(get_the_ID(), '_linkage_text', true);
                    $linkage_link = get_post_meta(get_the_ID(), '_linkage_link', true);
                    $linkage_option = get_post_meta(get_the_ID(), '_linkage_option', true); 
                    if($doc) {
                      $linkage_url = wp_get_attachment_url($doc);
                    } elseif($linkage_link) {
                      $linkage_url = $linkage_link;
                    } elseif(is_post_type('application')) {
                      $linkage_url = null;
                    } else {
                      $linkage_url = get_the_permalink();
                    }

                    // Open in new tab if document uploader or new window selected
                    if($doc) {
                      $target = ' target="_blank"';
                    } elseif($linkage_option == 'new') {
                      $target = ' target="_blank"';
                    } else {
                      $target = null;
                    }

                    // Popup if popup selected and document is empty
                    if($doc) {
                      $popup = null;
                    } elseif($linkage_option == 'popup') {
                      $popup = ' class="js-popup"';
                    } else {
                      $popup = null;
                    }
                    $title = get_the_title();
                ?>
                <li><a class="<?= $popup ?>" target="<?= $target?>" href="<?= $linkage_url ?>"><?= $title; ?></a></li>
                <?php
            }
          ?>
        </ul>

      <?php 
    }
    wp_reset_postdata();
}

function loadmore_posts() {

    global $wp_query;

  //    wp_enqueue_script('jquery');

    // register our main script but do not enqueue it yet
    wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/assets/scripts/myloadmore.js', array('jquery') );

    // now the most interesting part
    // we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
    wp_localize_script( 'my_loadmore', 'loadmore_posts_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
        'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
        'max_page' => $wp_query->max_num_pages
    ) );

    wp_enqueue_script( 'my_loadmore' );
}

add_action( 'wp_enqueue_scripts', 'loadmore_posts');

function loadmore_ajax_handler(){

    // prepare our arguments for the query
    $args = json_decode( stripslashes( $_POST['query'] ), true );
    $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_status'] = 'publish';

    // it is always better to use WP_Query but not here
    query_posts( $args );

    if( have_posts() ) :

        // run the loop
        while( have_posts() ): the_post();

            get_template_part('templates/content', get_post_type());


        endwhile;

    endif;
    die;
}

add_action('wp_ajax_loadmore', 'loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler');

//Add Extra Box if needed
// add_action('admin_init', 'details_add_meta_boxes', 2);

function details_add_meta_boxes() {
  add_meta_box( 'detailsinvoice-group', 'Details', 'details_repeatable_meta_box_display', 'page', 'normal', 'default');
}

function details_repeatable_meta_box_display() {
    global $post;
    $detailsinvoice_group = get_post_meta($post->ID, 'detailsdata_group', true);
     wp_nonce_field( 'details_repeatable_meta_box_nonce', 'details_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function( $ ){
          $( '#details-add-row' ).on('click', function() {
              var row = $( '.details-empty-row.screen-reader-text' ).clone(true);
              row.removeClass( 'details-empty-row screen-reader-text' );
              row.insertBefore( '#repeatable-fieldset-details tbody>tr:last' );
              return false;
          });

          $( '.details-remove-row' ).on('click', function() {
              $(this).parents('tr').remove();
              return false;
          });
      });
    </script>
    <table id="repeatable-fieldset-details" width="100%">
      <tbody>
        <?php
        if ( $detailsinvoice_group ) :
          foreach ( $detailsinvoice_group as $field ) {
        ?>
        <tr>
          <td width="15%">
            <input type="text"  placeholder="Title" name="detailsTitleItem[]" value="<?php if($field['detailsTitleItem'] != '') echo esc_attr( $field['detailsTitleItem'] ); ?>" /></td>
          <td width="70%">
          <textarea placeholder="Description" cols="55" rows="5" name="detailsTitledescription[]"> <?php if ($field['detailsTitledescription'] != '') echo esc_attr( $field['detailsTitledescription'] ); ?> </textarea></td>
          <td width="15%"><a class="button details-remove-row" href="#1">Remove</a></td>
        </tr>
        <?php
        }
        else :
        // show a blank one
        ?>
        <tr>
          <td>
            <input type="text" placeholder="Title" title="Title" name="detailsTitleItem[]" /></td>
          <td>
              <textarea  placeholder="Description" name="detailsTitledescription[]" cols="55" rows="5">  </textarea>
              </td>
          <td><a class="button  details-remove-row-button button-disabled" href="#">Remove</a></td>
        </tr>
        <?php endif; ?>

        <!-- empty hidden one for jQuery -->
        <tr class="details-empty-row screen-reader-text">
          <td>
            <input type="text" placeholder="Title" name="detailsTitleItem[]"/></td>
          <td>
              <textarea placeholder="Description" cols="55" rows="5" name="detailsTitledescription[]"></textarea>
              </td>
          <td><a class="button details-remove-row" href="#">Remove</a></td>
        </tr>
      </tbody>
    </table>
    <p><a id="details-add-row" class="button" href="#">Add another</a></p>
    <?php
}

add_action('save_post', 'details_repeatable_meta_box_save');


function details_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['details_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['details_repeatable_meta_box_nonce'], 'details_repeatable_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    $old = get_post_meta($post_id, 'detailsdata_group', true);
    $new = array();
    $detailsInvoiceItems = $_POST['detailsTitleItem'];
    $detailsprices = $_POST['detailsTitledescription'];
     $count = count( $detailsInvoiceItems );
     for ( $i = 0; $i < $count; $i++ ) {
        if ( $detailsInvoiceItems[$i] != '' ) :
            $new[$i]['detailsTitleItem'] = stripslashes( strip_tags( $detailsInvoiceItems[$i] ) );
             $new[$i]['detailsTitledescription'] = stripslashes( $detailsprices[$i] ); // and however you want to sanitize
        endif;
    }
    if ( !empty( $new ) && $new != $old )
        update_post_meta( $post_id, 'detailsdata_group', $new );
    elseif ( empty($new) && $old )
        delete_post_meta( $post_id, 'detailsdata_group', $old );


}

// Add the Custom Slider Box
function add_custom_meta_box() {
    add_meta_box(
        'custom_meta_box', // $id
        'Slide Selector', // $title 
        'show_custom_meta_box', // $callback
        'slide', // $page
        'normal', // $context
        'high'
    ); // $priority
}
add_action('add_meta_boxes', 'add_custom_meta_box');

// Field Array
$prefix = 'management_team-';
$communitys = get_posts([
    'posts_per_page' => -1,
    'post_type'      => 'page',
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'post_status' => 'publish',
    'post_parent' => 0
]);
$custom_meta_fields = array();
$x = 1;
foreach($communitys as $option) {
  $customguess = array(
      'label'=> $option->post_title,
      'desc'  => get_the_date('Y/m/d',$option->ID),
      'id'    => $prefix.$x,
      'type'  => 'checkbox',
      'value' => $option->ID
  );
  array_push($custom_meta_fields, $customguess);
  $x++;
};

// The Callback
function show_custom_meta_box() {
    global $custom_meta_fields, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
    // Begin the field table and loop
    echo '<ul class="jac-metabox-checkboxes">';
    foreach ($custom_meta_fields as $field) {
        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<li>';
                switch($field['type']) {
                  // checkbox
                  case 'checkbox':
                      echo '<input type="checkbox" value="'.$field['value'].'" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>';
                  break;
                } //end switch
        echo '<label for="'.$field['id'].'"><strong>'.$field['label'].'</strong><small>'.$field['desc'].'</small></strong></label></li>';
    } // end foreach
    echo '</ul>'; // end table
}

// Save the Data
function save_custom_meta($post_id) {
    global $custom_meta_fields;
     
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) 
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
     
    // loop through fields and save the data
    foreach ($custom_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach
}
add_action('save_post', 'save_custom_meta');


// Add the Custom Slider Box
function add_custom_meta_box_notification() {
    add_meta_box(
        'custom_meta_box_notification', // $id
        'Notification Bar Selector', // $title 
        'show_custom_meta_box_notification', // $callback
        'notification_bar', // $page
        'normal', // $context
        'high'
    ); // $priority
}
add_action('add_meta_boxes', 'add_custom_meta_box_notification');

// Field Array
$prefix_notification = '_notification-';
$communitys_notification = get_posts([
    'posts_per_page' => -1,
    'post_type'      => 'page',
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'post_status' => 'publish',
    'post_parent' => 0
]);
$custom_meta_fields_notification = array();
$x = 1;
foreach($communitys_notification as $option) {
  $customguess_notification = array(
      'label'=> $option->post_title,
      'desc'  => get_the_date('Y/m/d',$option->ID),
      'id'    => $prefix.$x,
      'type'  => 'checkbox',
      'value' => $option->ID
  );
  array_push($custom_meta_fields_notification, $customguess_notification);
  $x++;
};

// The Callback
function show_custom_meta_box_notification() {
    global $custom_meta_fields, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
    // Begin the field table and loop
    echo '<ul class="jac-metabox-checkboxes">';
    foreach ($custom_meta_fields as $field) {
        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<li>';
                switch($field['type']) {
                  // checkbox
                  case 'checkbox':
                      echo '<input type="checkbox" value="'.$field['value'].'" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>';
                  break;
                } //end switch
        echo '<label for="'.$field['id'].'"><strong>'.$field['label'].'</strong><small>'.$field['desc'].'</small></strong></label></li>';
    } // end foreach
    echo '</ul>'; // end table
}

// Save the Data
function save_custom_meta_notification($post_id) {
    global $custom_meta_fields_notification;
     
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) 
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
     
    // loop through fields and save the data
    foreach ($custom_meta_fields_notification as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach
}
add_action('save_post', 'save_custom_meta_notification');

// Add the Custom Slider Box
function add_custom_meta_box_video() {
    add_meta_box(
        'custom_meta_box_video', // $id
        'Video Popup Selector', // $title 
        'show_custom_meta_box_video', // $callback
        'video_popup', // $page
        'normal', // $context
        'high'
    ); // $priority
}
add_action('add_meta_boxes', 'add_custom_meta_box_video');

// Field Array
$prefix_video = 'video-';
$videopopups = get_posts([
    'posts_per_page' => -1,
    'post_type'      => 'page',
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'post_status' => 'publish',
    'post_parent' => 0
]);
$custom_meta_fields_video = array();
$x = 1;
foreach($videopopups as $option) {
  $customguessvideo = array(
      'label'=> $option->post_title,
      'desc'  => get_the_date('Y/m/d',$option->ID),
      'id'    => $prefix_video.$x,
      'type'  => 'checkbox',
      'value' => $option->ID
  );
  array_push($custom_meta_fields_video, $customguessvideo);
  $x++;
};

// The Callback
function show_custom_meta_box_video() {
    global $custom_meta_fields_video, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
    // Begin the field table and loop
    echo '<ul class="jac-metabox-checkboxes">';
    foreach ($custom_meta_fields_video as $field) {
        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<li>';
                switch($field['type']) {
                  // checkbox
                  case 'checkbox':
                      echo '<input type="checkbox" value="'.$field['value'].'" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>';
                  break;
                } //end switch
        echo '<label for="'.$field['id'].'"><strong>'.$field['label'].'</strong><small>'.$field['desc'].'</small></strong></label></li>';
    } // end foreach
    echo '</ul>'; // end table
}

// Save the Data
function save_custom_meta_video($post_id) {
    global $custom_meta_fields_video;
     
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) 
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
     
    // loop through fields and save the data
    foreach ($custom_meta_fields_video as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach
}
add_action('save_post', 'save_custom_meta_video');