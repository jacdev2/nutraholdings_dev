��          �       \      \  C   ]      �      �     �     �     �  
   	  	             >  0   E     v     ~  <   �     �     �     �     �  <      s  =  S   �  !     $   '     L     e     j     ~  
   �      �  
   �  E   �  	          L        l      �     �     �  $   �   <p>Need support? Please use the comment function on codecanyon.</p> Add some javascript if you want. Add some stylesheet if you want. Advanced settings All Apply for users Custom CSS Custom JS Custom stylesheet / javascript. Enable Enable variations table to use the options below General Information Need support? Please use the comment function on codecanyon. Only Logged in Only NOT logged in Price Variations Table Which user group should be affected by the variations table. Project-Id-Version: WooCommerce Product Catalog
POT-Creation-Date: 2019-10-02 13:50+0000
PO-Revision-Date: 2019-10-02 13:50+0000
Last-Translator: WeLaunch <support@dev.welaunch.io>
Language-Team: German
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: admin/redux-framework
X-Poedit-SearchPathExcluded-1: admin/redux-extensions
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.3.0; wp-5.2.3 <p>Brauchen Sie Hilfe? Dann schildern Sie uns bitte Ihr Problem auf Codecanyon.</p> Gib dem Plugin eigene Funktionen. Gib dem Plugin dein eigenes Styling. Erweiterte Einstellungen Alle Anwenden auf Nutzer Eigenes CSS Eigenes JS Eigenes Stylesheet / Javascript. Aktivieren Aktiviere den Katalog-Modus um die Optionen unten benutzen zu können Allgemein Informationen Brauchen Sie Hilfe? Dann schildern Sie uns bitte Ihr Problem auf Codecanyon. Nur eingeloggte User Nur „NICHT“ eingeloggte User Preis Katalog Modus Welche Nutzer sollen betroffen sein? 